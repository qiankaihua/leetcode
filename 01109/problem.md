# Corporate Flight Bookings

[题目链接](https://leetcode.com/problems/corporate-flight-bookings/)

<diff>Medium</diff>

给一个数组，数组每一项`<a,b,c>`表示`[a,b]`这个天数范围内，预定了 c 个，给出n，需要计算1-n天每一天的预定数量。
