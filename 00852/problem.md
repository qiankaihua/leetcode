# Peak Index in a Mountain Array

[题目链接](https://leetcode.com/problems/peak-index-in-a-mountain-array/submissions/)

<diff>Easy</diff>

给一个先增后减（保证）的数组，求出顶峰。