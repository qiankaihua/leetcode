# People Whose List of Favorite Companies Is Not a Subset of Another List

[题目链接](https://leetcode.com/problems/people-whose-list-of-favorite-companies-is-not-a-subset-of-another-list/)

<diff>Medium</diff>

给一个字符串集合的集合，问哪几个下标是别的下标的子集合