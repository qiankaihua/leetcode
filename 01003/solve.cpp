class Solution {
public:
    bool isValid(string S) {
        int cnt = 0, size = S.length();
        if (size < 3) return false;
        for (int i = 0; i < size; ++i) {
            switch (S[i]) {
                case 'a':
                    S[cnt] = 'a';
                    cnt++;
                    break;
                case 'b':
                    if (cnt == 0 || S[cnt - 1] != 'a') return false;
                    S[cnt] = 'b';
                    cnt++;
                    break;
                case 'c':
                    if (cnt < 2) return false;
                    if (S[cnt - 1] != 'b' || S[cnt - 2] != 'a') return false;
                    cnt -= 2;
                    break;
            }
        }
        return cnt == 0;
    }
};