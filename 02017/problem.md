# Grid Game

[题目链接](https://leetcode.com/problems/grid-game/)

<diff>Medium</diff>

给一个数组 `2 * N` 的数组, 每个数组格中的值代表可以获取的 value
两个机器人, 从 0, 0 走到 1, N-1,只能朝下或者朝右走
第一个机器人的目标是让第二个机器人拿到的最大价值最小, 走过的格子会把 value 变为 0
问第二个机器人拿到的价值最大是多少
