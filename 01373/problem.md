# Maximum Sum BST in Binary Tree

[题目链接](https://leetcode.com/problems/maximum-sum-bst-in-binary-tree/)

<diff>Hard</diff>

给一颗二叉树，计算所有是二叉搜索树的子树中最大的子树和。