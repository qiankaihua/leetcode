impl Solution {
    pub fn can_partition(nums: Vec<i32>) -> bool {
        let mut sum = 0;
        for i in nums.iter() {
            sum += i;
        }
        if sum % 2 != 0 {
            return false;
        }
        let mut dp = vec![false; (sum / 2 + 1) as usize];
        dp[0] = true;
        sum /= 2;
        for num in nums.iter() {
            for idx in (0..dp.len()).rev() {
                if dp[idx] {
                    if idx + (*num as usize) > sum as usize {
                        continue;
                    }
                    dp[idx + (*num as usize)] = true;
                }
            }
        }
        return dp[sum as usize];
    }
}
