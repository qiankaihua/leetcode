struct range {
    int r, l, t, b;
    bool mask[66];
    vector<int> out;
    int cnt, id;
    bool handle;
};
class Solution {
public:
    bool isPrintable(vector<vector<int>>& targetGrid) {
        int cnt = 0;
        unordered_map<int, range> mp;
        for (int i = 0; i < targetGrid.size(); ++i) {
            for (int j = 0; j < targetGrid[i].size(); ++j) {
                auto iter = mp.find(targetGrid[i][j]);
                if (iter == mp.end()) {
                    mp[targetGrid[i][j]] = {.r = j, .l = j, .t = i, .b = i, .cnt = 0, .id = targetGrid[i][j], .handle = false};
                    cnt++;
                } else {
                    iter->second.r = max(j, iter->second.r);
                    iter->second.l = min(j, iter->second.l);
                    iter->second.t = min(i, iter->second.t);
                    iter->second.b = max(i, iter->second.b);
                }
            }
        }
        for (int i = 0; i < targetGrid.size(); ++i) {
            for (int j = 0; j < targetGrid[i].size(); ++j) {
                auto iter = mp.find(targetGrid[i][j]);
                for (auto it = mp.begin(); it != mp.end(); it++) {
                    if (it->second.r >= j && it->second.l <= j && it->second.t <= i && it->second.b >= i && it->first != iter->first) {
                        if (it->second.mask[iter->first] == false) {
                            it->second.mask[iter->first] = true;
                            it->second.cnt += 1;
                            iter->second.out.push_back(it->first);
                        }
                    }
                }
            }
        }
        bool move = false;
        while (cnt > 0) {
            // print(cnt, mp);
            move = false;
            for (auto it = mp.begin(); it != mp.end(); it++) {
                if (it->second.cnt == 0 && !it->second.handle) {
                    it->second.handle = true;
                    move = true;
                    --cnt;
                    for (int i = 0; i < it->second.out.size(); ++i) {
                        auto iter = mp.find(it->second.out[i]);
                        iter->second.cnt--;
                    }
                }
            }
            if (!move) return false;
        }
        return true;
    }
    void print(int cnt, unordered_map<int, range> &mp) {
        cout << "----- cnt: " << cnt << " -----" << endl;
        for (auto it = mp.begin(); it != mp.end(); it++) {
            printf("id = %d, cnt = %d, vec=", it->second.id, it->second.cnt);
            for (int i = 0; i < it->second.out.size(); ++i) {
                cout << (it->second.out[i]) << ' ';
            }
            cout << endl;
        }
    }
};