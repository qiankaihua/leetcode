class Solution {
public:
    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
        sort(candidates.begin(), candidates.end());
        vector<vector<int>> ans;
        vector<int> tmp;
        dfs(candidates, target, 0, 0, ans, tmp);
        return ans;
    }
    void dfs(vector<int>& candidates, int target, int pos, int now, vector<vector<int>>& ans, vector<int> &tmp) {
        if (pos == candidates.size()) return;
        
        tmp.emplace_back(candidates[pos]);
        if (now + candidates[pos] == target) {
            ans.emplace_back(tmp);
            tmp.pop_back();
            return;
        }
        if (now + candidates[pos] < target) {
            dfs(candidates, target, pos + 1, now + candidates[pos], ans, tmp);
        }
        tmp.pop_back();
        int nxt = pos;
        while (++nxt < candidates.size()) {
            if (candidates[nxt] != candidates[pos]) break;
        }
        dfs(candidates, target, nxt, now, ans, tmp);
    }
};