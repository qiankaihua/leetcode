@echo off

py getReadme.py

git add . > nul

set nowtime=%DATE:~0,10% %TIME:~0,8% > nul

git commit -m "updated at %nowtime%" > test/a.tmp 2>&1

set /p first=< test/a.tmp
echo %first%

git push origin master > test/commit.tmp 2>&1

set /P file=<test/commit.tmp
echo %file%
