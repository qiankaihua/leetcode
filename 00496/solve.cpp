class Solution {
public:
    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2) {
        vector<int>dp(1e4 + 1, -1);
        stack<int> st;
        int tmp;
        for (int i = nums2.size() - 1; i >= 0; --i) {
            while (!st.empty() && st.top() < nums2[i]) {
                st.pop();
            }
            if (st.empty()) {
                dp[nums2[i]] = -1;
                st.push(nums2[i]);
            } else {
                dp[nums2[i]] = st.top();
                st.push(nums2[i]);
            }
        }
        vector<int> ans(nums1.size());
        for (int i = 0; i < nums1.size(); ++i) {
            ans[i] = dp[nums1[i]];
        }
        return std::move(ans);
    }
};