# Add Digits

[题目链接](https://leetcode.com/problems/add-digits/)

<diff>Easy</diff>

输入一个非负数n，计算n各个位之和，如果不是个位数，则重复这个过程。返回结果。

在O(1)的时间完成
