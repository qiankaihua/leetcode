struct node {
    int now;
    int pos;
    node(int _now, int _x, int _y): now(_now), pos(_x * 10000 + _y) {}
    void getPos(int* x, int* y) {
        *y = pos % 10000;
        *x = pos / 10000;
    }
    bool operator<(const node& r) const {
        return now > r.now;
    }
};
inline int getTime(int now, int required) {
    if (now >= required) return now;
    return required + ((required - now) & 1);
}
class Solution {
public:
    int minimumTime(vector<vector<int>>& grid) {
        if (grid[0][1] > 1 && grid[1][0] > 1) {
            return -1;
        }
        priority_queue<node> pq;
        pq.push(node(0, 0, 0));
        node now(0, 0, 0);
        int x, y, n = grid.size(), m = grid[0].size();
        vector<vector<bool>> flag(n, vector<bool>(m, true));
        flag[0][0] = false;
        while (!pq.empty()) {
            now = pq.top();
            pq.pop();
            now.getPos(&x, &y);
            if (x == n - 1 && y == m - 1) {
                return now.now;
            }
            if (x > 0 && flag[x - 1][y]) {
                flag[x - 1][y] = false;
                pq.push(node(getTime(now.now + 1, grid[x - 1][y]), x - 1, y));
            }
            if (x < n - 1 && flag[x + 1][y]) {
                flag[x + 1][y] = false;
                pq.push(node(getTime(now.now + 1, grid[x + 1][y]), x + 1, y));
            }
            if (y > 0 && flag[x][y - 1]) {
                flag[x][y - 1] = false;
                pq.push(node(getTime(now.now + 1, grid[x][y - 1]), x, y - 1));
            }
            if (y < m - 1 && flag[x][y + 1]) {
                flag[x][y + 1] = false;
                pq.push(node(getTime(now.now + 1, grid[x][y + 1]), x, y + 1));
            }
        }
        return -1;
    }
};