class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int S) {
        int sum = 0, upper;
        for (int i = 0; i < nums.size(); ++i) {
            sum += nums[i];
        }
        if (S < -1 * sum || S > sum) return 0;
        vector<vector<int>> dp(nums.size() + 1, vector<int>((sum + 1) * 2, 0));
        dp[1][sum - nums[0]] += 1;
        dp[1][sum + nums[0]] += 1;
        for (int i = 0; i < nums.size(); ++i) {
            upper = 2 * sum;
            for (int j = nums[i]; j <= upper; ++j) {
                dp[i + 1][j - nums[i]] += dp[i][j];
            }
            upper = 2 * sum - nums[i];
            for (int j = 0; j <= upper; ++j) {
                dp[i + 1][j + nums[i]] += dp[i][j];
            }
        }
        return dp[nums.size()][sum + S];
    }
};