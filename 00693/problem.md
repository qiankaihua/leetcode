# Binary Number with Alternating Bits

[题目链接](https://leetcode.com/problems/binary-number-with-alternating-bits/)

<diff>Easy</diff>

给一个正整数，检查他们的二进制表达式是否是01交错的，如果是，返回true。

case1:
input: 5, output: True

case2:
input: 7, output: False
