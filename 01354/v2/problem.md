# Construct Target Array With Multiple Sums

[题目链接](https://leetcode.com/problems/construct-target-array-with-multiple-sums/)

<diff>Hard</diff>

给一个长度为 n 的 vector, 判断是否可以通过长度为 n 的全 1 数据通过若干次操作得到, 每次操作可以指定一个位置, 使得这个位置的数等于当前数字的总和, 例如:

给定 `[8, 5]`

可以通过如下序列得到:

```
[1, 1] =>
[1, 2] =>
[3, 2] =>
[3, 5] =>
[8. 5]
```