class Solution {
public:
    bool isSub(string a, string b) {
        return b.find(a) != string::npos;
    }
    vector<string> stringMatching(vector<string>& words) {
        vector<string> ans;
        vector<bool> flag(words.size(), false);
        for (int i = 0; i < words.size(); ++i) {
            for (int j = i + 1; j < words.size(); ++j) {
                if (words[i].length() < words[j].length()) {
                    if (!flag[i] && isSub(words[i], words[j])) {
                        ans.push_back(words[i]);
                        flag[i] = true;
                    }
                } else {
                    if (!flag[j] && isSub(words[j], words[i])) {
                        ans.push_back(words[j]);
                        flag[j] = true;
                    }
                }
            }
        }
        return ans;
    }
};