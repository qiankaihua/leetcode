# Minimum Falling Path Sum

[题目链接](https://leetcode.com/problems/minimum-falling-path-sum/)

<diff>Medium</diff>

给一个矩阵，从最上面走到最下面，可以往左下一格、下面一格、右下一格行走，每次走到一个格子都会加上该格子上面的数值，问最小和是多少
