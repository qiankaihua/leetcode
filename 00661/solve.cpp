class Solution {
public:
    vector<vector<int>> imageSmoother(vector<vector<int>>& M) {
        int n = M.size();
        if (n == 0) return M;
        int m = M[0].size();
        int sum, cnt;
        const int mask = 0xff;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                sum = (M[i][j] & mask);
                cnt = 1;
                if (i != 0) {
                    sum += (M[i - 1][j] & mask);
                    ++cnt;
                    if (j != 0) {
                        sum += (M[i - 1][j - 1] & mask);
                        ++cnt;
                    }
                    if (j != m - 1) {
                        sum += (M[i - 1][j + 1] & mask);
                        ++cnt;
                    }
                }
                if (j != 0) {
                    sum += (M[i][j - 1] & mask);
                    ++cnt;
                }
                if (j != m - 1) {
                    sum += (M[i][j + 1] & mask);
                    ++cnt;
                }
                if (i != n - 1) {
                    sum += (M[i + 1][j] & mask);
                    ++cnt;
                    if (j != 0) {
                        sum += (M[i + 1][j - 1] & mask);
                        ++cnt;
                    }
                    if (j != m - 1) {
                        sum += (M[i + 1][j + 1] & mask);
                        ++cnt;
                    }
                }
                M[i][j] += ((sum / cnt) << 8);
            }
        }
        
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                M[i][j] >>= 8;
            }
        }
        return M;
    }
};