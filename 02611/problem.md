# Mice and Cheese

[题目链接](https://leetcode.com/problems/mice-and-cheese)

<diff>Medium</diff>

给两个数组, 代表两只老鼠吃同一个芝士获得的心情值, 第三个数 N, 代表第一只老鼠要吃 N 个芝士. 求出两只老鼠加起来最多获得多少心情值

数组长度 / N 范围为 [1, 1e5]