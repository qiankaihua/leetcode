class Solution {
public:
    int singleNonDuplicate(vector<int>& nums) {
        int l = 0, r = nums.size() - 1, m, n;
        while (l < r) {
            m = l + (r - l) / 2;
            if (m % 2 == 0) n = m + 1;
            else n = m - 1;
            if (nums[n] == nums[m]) {
                l = n > m ? n + 1 : m + 1;
            } else {
                r = n < m ? n - 1 : m - 1;
            }
        }
        return nums[l];
    }
};