class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int res = 1, cnt = 1, last = nums[0];
        for (int i = 1; i < nums.size(); ++i) {
            if (last == nums[i]) {
                ++cnt;
                if (cnt > 2) continue;
            } else {
                cnt = 1;
                last = nums[i];
            }
            nums[res++] = nums[i];
        }
        return res;
    }
};