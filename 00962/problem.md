# Find the Kth Largest Integer in the Array

[题目链接](https://leetcode.com/problems/maximum-width-ramp/)

<diff>Medium</diff>

给一串整数(5*1e4个)，找出所有符合 `i < j && num[i] < num[j]` 条件的 `i`/ `j` 对中， `j - i` 最大值
