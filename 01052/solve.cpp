class Solution {
public:
    int maxSatisfied(vector<int>& customers, vector<int>& grumpy, int X) {
        int now = 0, ans = 0, m = customers.size();
        for (int i = 0; i < m; ++i) {
            now += grumpy[i] == 1 ? 0 : customers[i];
        }
        for (int i = 0; i < X && i < m; ++i) {
            now += grumpy[i] == 1 ? customers[i] : 0;
        }
        if (X >= m) return now;
        ans = now;
        for (int i = X; i < customers.size(); ++i) {
            now += grumpy[i] == 1 ? customers[i] : 0;
            now -= grumpy[i - X] == 1 ? customers[i - X] : 0;
            ans = max(ans, now);
        }
        return ans;
    }
};