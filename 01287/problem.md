# Element Appearing More Than 25% In Sorted Array

[题目链接](https://leetcode.com/problems/element-appearing-more-than-25-in-sorted-array)

<diff>Easy</diff>

给一个vector，找出其中出现 25% 以上次数的数, 保证有且只有一个