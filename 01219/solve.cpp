class Solution {
public:
    int g_max = 0;
    int getMaximumGold(vector<vector<int>>& grid) {
        g_max = 0;
        int n, m;
        n = grid.size();
        if (n == 0) return 0;
        m = grid[0].size();
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                dfs(0, i, j, n, m, grid);
            }
        }
        return g_max;
    }
    int dx[4] = {0, 0, 1, -1};
    int dy[4] = {1, -1, 0, 0};
    void dfs(int now, int x, int y, int n, int m, vector<vector<int>>& grid) {
        if (x < 0 || y < 0 || x >= n || y >= m || grid[x][y] == 0) {
            g_max = max(g_max, now);
            return;
        }
        int bak = grid[x][y];
        grid[x][y] = 0;
        for (int i = 0; i < 4; ++i) {
            dfs(now + bak, x + dx[i], y + dy[i], n, m, grid);
        }
        grid[x][y] = bak;
    }
};