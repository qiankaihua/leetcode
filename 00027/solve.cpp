class Solution {
public:
    int removeElement(vector<int>& nums, int val) {
        int cnt = 0, sum = nums.size();
        for (int i = 0; i < sum; ++i) {
            if (nums[i] != val) {
                nums[cnt++] = nums[i];
            }
        }
        return cnt;
    }
};