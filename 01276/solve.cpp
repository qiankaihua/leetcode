class Solution {
public:
    vector<int> numOfBurgers(int tomatoSlices, int cheeseSlices) {
        vector<int>ans;
        if (tomatoSlices & 1) return ans;
        int maxx = tomatoSlices >> 1;
        int diff = maxx - cheeseSlices;
        if (diff < 0 || diff * 2 > maxx) return ans;
        ans.push_back(diff);
        ans.push_back(maxx - 2 * diff);
        return ans;
    }
};