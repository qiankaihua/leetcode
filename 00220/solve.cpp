class Solution {
public:
    bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
        if (nums.size() == 0) return false;
        map<long long, int> mp;
        mp[nums[0]] = 1;
        for (int i = 1; i < nums.size(); ++i) {
            if (i > k) {
                --mp[nums[i - k - 1]];
                if (mp[nums[i - k - 1]] == 0) {
                    mp.erase(nums[i - k - 1]);
                }
            }
            auto it = mp.lower_bound((long long)nums[i] - t);
            if (it != mp.end()) {
                if (it->first <= (long long)nums[i] + t) {
                    return true;
                }
            }
            if (mp.find(nums[i]) == mp.end()) {
                mp[nums[i]] = 1;
            } else {
                ++mp[nums[i]];
            }
        }
        return false;
    }
};