# Dota2 Senate

[题目链接](https://leetcode.com/problems/dota2-senate/)

<diff>Medium</diff>

给一个字符串，包含“R”和“D”，代表天辉和夜魔的参议员，每个参议员有两个权利

1. 禁止某个参议员的权利
2. 宣布胜利

一方获胜当且仅当一轮中所有人都同意一方胜利，每个人都采取最佳策略，问最后谁能赢。
