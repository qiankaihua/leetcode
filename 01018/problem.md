# Binary Prefix Divisible By 5

[题目链接](https://leetcode.com/problems/binary-prefix-divisible-by-5/)

<diff>Easy</diff>

给一个01数组，代表二进制，问你从`A[0]`到`A[i]`组成的数能否被5整除
