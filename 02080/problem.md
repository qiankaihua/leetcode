# Range Frequency Queries

[题目链接](https://leetcode.com/problems/range-frequency-queries/)

<diff>Medium</diff>

初始化给你一个数组 arr, 数组长度为 N, 每一项 value 属于 `[1, 1e4]`
后面会有 q 次查询, 每次给 left, right, value, 查询在 `[left, right]` 区间内, value 出现了多少次

`0 <= left <= right < N`
`q <= 1e5`
