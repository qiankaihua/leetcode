# Can Place Flowers

[题目链接](https://leetcode.com/problems/can-place-flowers/)

<diff>Easy</diff>

给一个数组 num, 和一个 n, 数组 1 表明有花种着, 花之间不能相邻, 问是否还有 n 朵花可以种.