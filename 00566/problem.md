# Reshape the Matrix

[题目链接](https://leetcode.com/problems/reshape-the-matrix/submissions/)

<diff>Easy</diff>

将一个二维数组变成一个另一个形状，如果不合法，则返回原数组。