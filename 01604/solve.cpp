class Solution {
public:
    int maximumRequests(int n, vector<vector<int>>& requests) {
        vector<int> npos(n, 0);
        return dfs(requests, npos, 0, 0);
    }
    int dfs(const vector<vector<int>>& requests, vector<int>& npos, int pos, int pick) {
        if (pos == requests.size()) {
            for (int i = 0; i < npos.size(); ++i) {
                if (npos[i] != 0) return 0;
            }
            return pick;
        }
        npos[requests[pos][0]]--;
        npos[requests[pos][1]]++;
        int subRes = dfs(requests, npos, pos + 1, pick + 1);
        npos[requests[pos][1]]--;
        npos[requests[pos][0]]++;
        return max(subRes, dfs(requests, npos, pos + 1, pick));
    }
};