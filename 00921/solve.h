#ifdef LEETCODE_INCLUDE_TEST
#include "../0test/include.h"
#endif

class Solution {
public:
    int minAddToMakeValid(string s) {
        if (s.length() == 0) return 0;
        int res = 0, left = 0;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == ')') {
                if (left <= 0) {
                    ++res;
                } else {
                    --left;
                }
            } else {
                ++left;
            }
        }
        res += left;
        return res;
    }
};