class Solution {
public:
    bool canTransform(string start, string end) {
        int len = start.length(), now = -1, l = 0, r = 0;
        if (len != end.length()) return false;
        for (int i = 0; i < len; ++i) {
            if (start[i] == 'X') continue;
            if (start[i] == 'L') {
                ++l;
                for (++now; now <= i; ++now) {
                    if (end[now] == 'X') continue;
                    if (end[now] == 'L') break;
                    return false;
                }
                if (now > i) return false;
            } else {
                ++r;
                for (++now; now < i; ++now) {
                    if (end[now] == 'R' || end[now] == 'L') return false;
                }
                for (; now < len; ++now) {
                    if (end[now] == 'L') return false;
                    if (end[now] == 'R') break;
                }
                if (now == len) return false;
            }
        }
        for (int i = 0; i < len; ++i) {
            if (end[i] == 'R') --r;
            else if (end[i] == 'L') --l;
        }
        return l == 0 && r == 0;
    }
};