直接暴力，不过看到一个比较优质的解法，就是直接double字符串，然后移除最后一个字符，用原字符串在新的字符串中查找。

原理是，如果原字符串是符合条件的，则 `string = AAA...AAA`，而新构造的字符串是 `AAA...AAAA'`, `A'` 是 `A` 少一个字符，必然能找到匹配的点。

```cpp
bool sol2 (string s) {
    string s2 = s + s;
    s2.pop_back();
    return s2.find(s, 1) != string::npos;
}
```
