impl Solution {
    pub fn maximum_beauty(nums: Vec<i32>, k: i32) -> i32 {
        let mut vec = nums.clone();
        vec.sort();
        let mut i = 0 as usize;
        let mut res = 0 as i32;
        let mut j = 1;
        while i < vec.len() {
            let m = k * 2 + vec[i];
            while j < vec.len() {
                if vec[j] > m {
                    break;
                }
                j += 1;
            }
            res = res.max((j - i) as i32);
            i += 1;
        }
        res
    }
}

// solve 2
impl Solution {
    pub fn maximum_beauty(nums: Vec<i32>, k: i32) -> i32 {
        let n = (*nums.iter().max().unwrap() + 2) as usize;
        let mut cnt = vec![0; n];
        for v in nums {
            cnt[0.max(v - k) as usize] += 1;
            cnt[(n - 1).min((v + k + 1) as usize)] -= 1;
        }

        let (mut res, mut curr) = (0, 0);
        for v in cnt {
            curr += v;
            res = res.max(curr);
        }
        return res;
    }
}
