# Shift 2D Grid

[题目链接](https://leetcode.com/problems/shift-2d-grid/)

<diff>Easy</diff>

给一个二维数组和一个k，每一个数据都往后移动k个格子。

数据范围为-1000~1000
