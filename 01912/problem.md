# Design Movie Rental System

[题目链接](https://leetcode.com/problems/design-movie-rental-system/)

<diff>Hard</diff>

给一个 `N`, 代表有 `N` 家店，给一个`<shop_id, movie_id, price_id>`构成的数组`entry`，保证每家店最多只持有一种电影一张。

现在有下面几种操作：

1. 查找：找到指定电影没有被出租的最便宜的五家商店，如果价格相同，商店id小的排在前面
2. 租借：租指定商店的指定电影
3. 归还，将指定商店的指定电影归还，保证输入正确
4. 汇报：返回目前出租的最便宜的5个电影，以`shop_id, movie_id`的形式返回，价格相同的话商店id小的排前面，商店id相同电影id小的排前面

`1 <= n <= 3 * 10^5`

`1 <= len(entry) <= 10^5`

`1 <= movie_i, price_i <= 10^4 `

所有操作加起来最多`10^5`次
