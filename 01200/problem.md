# Minimum Absolute Difference

[题目链接](https://leetcode.com/problems/minimum-absolute-difference/)

<diff>Easy</diff>

给一个互不相同的数字组成的数组，然后求出差值最小的组合，返回所有组合。
