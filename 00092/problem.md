# Reverse Linked List II

[题目链接](https://leetcode.com/problems/reverse-linked-list-ii/)

<diff>Medium</diff>

给一个单向链表和n，m，翻转链表中m到n的元素

case：
```
input:
1->2->3->4->5->NULL, m = 2, n = 4
output:
1->4->3->2->5->NULL

```