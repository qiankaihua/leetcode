# Second Minimum Node In a Binary Tree

[题目链接](https://leetcode.com/problems/second-minimum-node-in-a-binary-tree/)

<diff>Easy</diff>

给你一棵树满足根节点是儿子节点的最小值，而且保证要么没有儿子节点，要么只有两个儿子节点，求出第二小的值
