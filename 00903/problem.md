# Valid Permutations for DI Sequence

[题目链接](https://leetcode.com/problems/valid-permutations-for-di-sequence/)

<diff>Hard</diff>

给一个长度为n的字符串S，包含'D'，'I'两个，代表decreasing和increasing

需要求出合法的[0, n-1]的排列个数，结果 mod 1e9+7

合法的排列满足一下条件：

- if `S[i] == 'D'` 那么 `P[i] > P[i + 1]`
- if `S[i] == 'I'` 那么 `P[i] < P[i + 1]`
