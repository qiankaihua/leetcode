class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        check.resize(10);
        for (int i = 0; i < 9; ++i) {
            if (!checkRow(board, i) || !checkCol(board, i) || !checkGrid(board, i)) {
                return false;
            }
        }
        return true;
    }
private:
    vector<int> check;
    inline bool checkRow(vector<vector<char>>& board, int idx) {
        reset();
        for (int i = 0; i < 9; ++i) {
            if (board[idx][i] != '.') {
                if (check[board[idx][i] - '0'] > 0) return false;
                check[board[idx][i] - '0'] = 1;
            }
        }
        return true;
    }
    inline bool checkCol(vector<vector<char>>& board, int idx) {
        reset();
        for (int i = 0; i < 9; ++i) {
            if (board[i][idx] != '.') {
                if (check[board[i][idx] - '0'] > 0) return false;
                check[board[i][idx] - '0'] = 1;
            }
        }
        return true;
    }
    inline bool checkGrid(vector<vector<char>>& board, int idx) {
        reset();
        int baseRow = (idx / 3) * 3, baseCol = (idx % 3) * 3;
        for (int i = 0; i < 9; ++i) {
            if (board[baseRow + i / 3][baseCol + i % 3] != '.') {
                if (check[board[baseRow + i / 3][baseCol + i % 3] - '0'] > 0) return false;
                check[board[baseRow + i / 3][baseCol + i % 3] - '0'] = 1;
            }
        }
        return true;
    }
    inline void reset() {
        fill(check.begin(), check.end(), 0);
    }
};