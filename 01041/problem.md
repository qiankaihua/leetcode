# Robot Bounded In Circle

[题目链接](https://leetcode.com/problems/robot-bounded-in-circle/)

<diff>Medium</diff>

给一个字符串，包含“G”，“L”，“R”三种字符，代表直走，左转，右转，问重复执行这个字符串会不会在平面上有圈。
