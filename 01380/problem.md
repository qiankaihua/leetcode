# Lucky Numbers in a Matrix

[题目链接](https://leetcode.com/problems/lucky-numbers-in-a-matrix/)

<diff>Easy</diff>

给一个矩阵，包含不同的数字，返回所有的lucky number，顺序不限

lucky number指的是这个数字是这一行的最小值同时也是这一列的最大值。
