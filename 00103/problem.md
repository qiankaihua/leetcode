# Binary Tree Zigzag Level Order Traversal

[题目链接](https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/)

<diff>Medium</diff>

给一颗树按照层来蛇形遍历

1
2 <- 3
4 -> 5 -> 6

