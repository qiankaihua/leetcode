# Find the Kth Largest Integer in the Array

[题目链接](https://leetcode.com/problems/find-the-kth-largest-integer-in-the-array/)

<diff>Medium</diff>

给一串字符串构成的大整数(1e4个)，找出第k大的数
