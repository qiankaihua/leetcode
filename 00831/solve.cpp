class Solution {
public:
    string maskPII(string S) {
        if (isEmail(S)) {
            return handleEmail(S);
        } else {
            return handleNumber(S);
        }
    }
    bool isEmail(string S) {
        for (int i = S.length() - 1; i >= 0; --i) {
            if (S[i] == '@') return true;
        }
        return false;
    }
    string handleEmail(string S) {
        transform(S.begin(),S.end(),S.begin(),::tolower);
        int pos;
        for (pos = 0; S[pos] != '@'; ++pos);
        string ans = "*******";
        ans[0] = S[0];
        ans[6] = S[pos - 1];
        ans += S.substr(pos);
        return ans;
    }
    string handleNumber(string S) {
        return formatNumber(getNumber(S));
    }
    vector<char> getNumber(string S) {
        vector<char> res;
        for (int i = 0; i < S.length(); ++i) {
            cout << S[i] << endl;
            if (S[i] >= '0' && S[i] <= '9') res.push_back(S[i]);
        }
        cout << res.size() << endl;
        return res;
    }
    string formatNumber(vector<char> v) {
        int size = v.size() - 4;
        string ans;
        switch(size) {
            case 6:
                ans = "***-***-****";
                break;
            case 7:
                ans = "+*-***-***-****";
                break;
            case 8:
                ans = "+**-***-***-****";
                break;
            default:
                ans = "+***-***-***-****";
                break;
        }
        int len = ans.length() - 4;
        for (int i = size; i < v.size(); ++i, ++len) {
            ans[len] = v[i];
        }
        return ans;
    }
};