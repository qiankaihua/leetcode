# Next Greater Element I

[题目链接](https://leetcode.com/problems/next-greater-element-i/)

<diff>Easy</diff>

给两个数组，第一个数组是第二个的子数组，求出第一个数组中每一个元素在第二个数组中在右边最近的比它大的元素是什么

```
Input: nums1 = [4,1,2], nums2 = [1,3,4,2]
Output: [-1,3,-1]
```

在 `O(len(arr1) + len(arr2))` 的时间内计算
