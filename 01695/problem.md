# Maximum Erasure Value

[题目链接](https://leetcode.com/problems/maximum-erasure-value/)

<diff>Medium</diff>

给一个数组，从中找出一个不包含重复数字的和最大的子数组，保证值都大于0