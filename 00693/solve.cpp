class Solution {
public:
    bool hasAlternatingBits(int n) {
        int last = n & 1;
        n >>= 1;
        while(n) {
            if (last == (n & 1)) return false;
            last = 1 - last;
            n >>= 1;
        }
        return true;
    }
};