# Minimum Speed to Arrive on Time

[题目链接](https://leetcode.com/problems/minimum-speed-to-arrive-on-time/)

<diff>Medium</diff>

给一个数组, 表示一列火车的运行距离, 再给一个 float, 表示需要在这个时间内坐完所有的火车, 每辆火车都在整点发车, 问火车最慢需要多少的运行速度才能满足需求, 如果不行, 返回 -1.

保证答案在 1e7 以内, 时间 float 最多两位小数

`1 <= n <= 1e5`
`1 <= dist[i] <= 1e5`
`1 <= hour <= 1e9`
