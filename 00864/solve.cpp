//#define debug
#define uint64 unsigned long long
class Solution {
private:
    struct Node {
        uint x, y, status, step;
        Node() {
            x = y = status = step = 0;
        }
        Node(int _x, int _y, int _status, int _step) {
            x = _x, y = _y, status = _status, step = _step;
        }
    };
    void getNodeFromUint(Node& n, uint64 i) {
        #ifdef debug
        cout << "turn to struct: " << i << endl;
        #endif
        n.step = (i >> 24);
        n.x = (i >> 16 & 0xFF);
        n.y = (i >> 8 & 0xFF);
        n.status = (i & 0xFF);
    }
    uint64 getUintFromNode(Node& n) {
        #ifdef debug
        cout << "turn from struct: " << n.x << ' ' << n.y << ' ' << n.status << ' ' << n.step << endl;
        #endif
        return ((uint64)(n.step) << 24) | (n.x << 16) | (n.y << 8) | n.status;
    }
    bool checkPoint(Node& n, Node& m) {
        return n.x == m.x && n.y == m.y;
    }
    bool checkNxt(vector<string>& grid, int n, int m, int nx, int ny, int status) {
        if (nx < 0 || nx >= n || ny < 0 || ny >= m) return false;
        if (grid[nx][ny] == '#') return false;
        if (grid[nx][ny] >= 'A' && grid[nx][ny] <= 'F') return (1 << (grid[nx][ny] - 'A')) & status;
        return true;
    }
public:
    int shortestPathAllKeys(vector<string>& grid) {
        int n = grid.size();
        if (n == 0) return 0;
        int m = grid[0].length();
        if (m == 0) return 0;
        bool vis[32][32][64];
        memset(vis, false, sizeof(vis));
        queue<uint64> q;
        Node start;//, key[6], lock[6];
        int key_status = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                // if (grid[i][j] >= 'A' && grid[i][j] <= 'F') {
                //     int k = grid[i][j] - 'A';
                //     lock[k].x = i;
                //     lock[k].y = j;
                // }
                if (grid[i][j] >= 'a' && grid[i][j] <= 'f') {
                    int k = grid[i][j] - 'a';
                //     key[k].x = i;
                //     key[k].y = j;
                    key_status |= (1 << k);
                }
                if (grid[i][j] == '@') {
                    start.x = i;
                    start.y = j;
                    start.status = 0;
                    start.step = 0;
                    vis[i][j][0] = true;
                    q.push(getUintFromNode(start));
                }
            }
        }
        Node now;
        int dx[4] = {1, -1, 0, 0}, dy[4] = {0, 0, 1, -1};
        while (!q.empty()) {
            getNodeFromUint(now, q.front());
            #ifdef debug
            cout << now.x << ' ' << now.y << ' ' << now.status << ' ' << now.step << endl;
            #endif
            if (now.status == key_status) {
                return now.step;
            }
            q.pop();
            for (int i = 0; i < 4; ++i) {
                Node nxt = Node(now.x + dx[i], now.y + dy[i], now.status, now.step + 1);
                if (checkNxt(grid, n, m, nxt.x, nxt.y, now.status)) {
                    if (grid[nxt.x][nxt.y] <= 'f' && grid[nxt.x][nxt.y] >= 'a') {
                        nxt.status |= (1 << (grid[nxt.x][nxt.y] - 'a'));
                    }
                    if (!vis[nxt.x][nxt.y][nxt.status]) {
                        vis[nxt.x][nxt.y][nxt.status] = true;
                        q.push(getUintFromNode(nxt));
                    }
                }
            }
        }
        return -1;
    }
};