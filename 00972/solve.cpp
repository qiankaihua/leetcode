int64_t gcd(int64_t x, int64_t y) {
    return y > 0 ? gcd(y, x % y) : x;
}
int64_t pow10(int x) {
    int64_t p = 1;
    for (int i = 0; i < x; ++i) {
        p *= 10;
    }
    return p;
}

struct fraction {
    int64_t numerator;
    int64_t denominator;

    fraction(const string &s) {
        int64_t integer = 0;
        int64_t nonRepeatingPart = 0, nonRepeatingPartLen = 0;
        int64_t repeatedPart = 0, repeatedPartLen = 0;
        bool isIntegerPart = true, isRepeatedPart = false;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == '.') {
                isIntegerPart = false;
                continue;
            }
            if (s[i] == '(') {
                isRepeatedPart = true;
                continue;
            }
            if (s[i] == ')') {
                continue;
            }
            if (isIntegerPart) {
                integer = integer * 10 + s[i] - '0';
            } else {
                if (isRepeatedPart) {
                    repeatedPart = repeatedPart * 10 + s[i] - '0';
                    ++repeatedPartLen;
                } else {
                    nonRepeatingPart = nonRepeatingPart * 10 + s[i] - '0';
                    ++nonRepeatingPartLen;
                }
            }
        }
        // nonrepeat part
        denominator = pow10(nonRepeatingPartLen);
        numerator = nonRepeatingPart;
        // repeat part
        if (repeatedPartLen > 0) {
            int repreatBase = pow10(repeatedPartLen) - 1;
            numerator = numerator * repreatBase + repeatedPart;
            denominator *= repreatBase;
        }
        // integer part
        numerator += denominator * integer;
        int gcd_ = gcd(numerator, denominator);
        numerator /= gcd_;
        denominator /= gcd_;
    }
};


class Solution {
public:
    bool isRationalEqual(string s, string t) {
        return fractionEqual(fraction(s), fraction(t));
    }
private:
    bool fractionEqual(const fraction &s, const fraction &t) {
        return s.denominator == t.denominator && s.numerator == t.numerator;
    }
};