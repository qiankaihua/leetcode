class Solution {
public:
    vector<int> luckyNumbers (vector<vector<int>>& matrix) {
        int rows = matrix.size(), cols;
        vector<int> ans;
        if (rows == 0) return ans;
        cols = matrix[0].size();
        vector<int> mins(rows, INT_MAX);
        vector<int> maxs(cols, INT_MIN);
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                mins[i] = min(mins[i], matrix[i][j]);
                maxs[j] = max(maxs[j], matrix[i][j]);
            }
        }
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                if (matrix[i][j] == mins[i] && matrix[i][j] == maxs[j]) {
                    ans.push_back(matrix[i][j]);
                }
            }
        }
        return ans;
    }
};