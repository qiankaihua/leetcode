/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
void getLayers(vector<int> &layers, TreeNode* root, int layer = 0) {
    if (root == nullptr) return;
    if (layers.size() <= layer) layers.emplace_back(0);
    layers[layer] += root->val;
    getLayers(layers, root->left, layer + 1);
    getLayers(layers, root->right, layer + 1);
}
void genNewTree(const vector<int> &layers, TreeNode* root, int extra, int layer = 0) {
    if (root == nullptr) return;
    int newExtra = 0;
    root->val = layers[layer] - extra;
    if (root->left != nullptr) newExtra += root->left->val;
    if (root->right != nullptr) newExtra += root->right->val;
    genNewTree(layers, root->left, newExtra, layer + 1);
    genNewTree(layers, root->right, newExtra, layer + 1);
}
class Solution {
public:
    TreeNode* replaceValueInTree(TreeNode* root) {
        if (root == nullptr) return root;
        vector<int> layers;
        getLayers(layers, root);
        genNewTree(layers, root, root->val);
        return root;
    }
};