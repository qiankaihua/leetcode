# Range Sum Query 2D - Immutable

[题目链接](https://leetcode.com/problems/range-sum-query-2d-immutable/)

<diff>Medium</diff>

给一个数组，然后后续有多个请求，询问 (row1, col1) 到 (row2, col2) 的数组元素之和
