class Solution {
public:
    bool check(vector<int> &a, vector<int> &b) {
        for (int i = 0; i < 26; ++i) {
            if (a[i] > b[i]) return false;
        }
        return true;
    } 
    bool checkIfCanBreak(string s1, string s2) {
        vector<int> v1 = vector<int>(26);
        vector<int> v2 = vector<int>(26);
        for (int i = s1.length() - 1; i >= 0; --i) {
            v1[s1[i] - 'a']++;
        }
        for (int i = s2.length() - 1; i >= 0; --i) {
            v2[s2[i] - 'a']++;
        }
        for (int i = 1; i < 26; ++i) {
            v1[i] += v1[i - 1];
            v2[i] += v2[i - 1];
        }
        return check(v1, v2) || check(v2, v1);
    }
};