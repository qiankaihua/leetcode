# Remove All Adjacent Duplicates In String

[题目链接](https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string/)

<diff>Easy</diff>

给你一个字符串，相邻相同的字母可以消去，返回最后剩下的字符串。
