# Design Browser History

[题目链接](https://leetcode.com/problems/design-browser-history/)

<diff>Medium</diff>

设计一个浏览器历史类，有几个功能

- 初始化函数
- 访问一个网页，会清空向前跳的历史
- 回退 N 步
- 向前跳 N 步
