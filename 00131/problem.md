# Palindrome Partitioning

[题目链接](https://leetcode.com/problems/palindrome-partitioning/)

<diff>Medium</diff>

给一个字符串，将他分割成多个回文串，找到所有可能的分割方法。
