# Stream of Characters

[题目链接](https://leetcode.com/problems/stream-of-characters/)

<diff>Hard</diff>

给一个字符串数组，然后每次查询输入一个字符，判断最后输入的N个字符构成的字符串是否存在在原有的字符串数组中。
