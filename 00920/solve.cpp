class Solution {
public:
    const long long  mod = 1e9+7;
    int numMusicPlaylists(int N, int L, int K) {
        if (N == 0) return 0;
        long long dp[101][101] = {0};
        dp[0][0] = 1;
        for (int i = 1; i <= L; ++i) {
            for (int j = 1; j <= i && j <= N; ++j) {
                dp[i][j] = dp[i - 1][j - 1] * (N - j + 1) % mod;
                dp[i][j] = (dp[i][j] + dp[i - 1][j] * (j - K > 0 ? j - K : 0) % mod) % mod;
            }
        }
        return dp[L][N];
    }
};