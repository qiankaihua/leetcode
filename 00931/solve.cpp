class Solution {
public:
    int minFallingPathSum(vector<vector<int>>& matrix) {
        int row = matrix.size();
        if (row == 0) return 0;
        int col = matrix[0].size();
        if (col == 0) return 0;
        vector<vector<int>> ans(2, vector<int>(col, 0));
        int pos = 1, nxtPos = 0, subMin;
        for (int i = 0; i < col; ++i) ans[0][i] = matrix[row - 1][i];
        for (int i = row - 2; i >= 0; --i) {
            for (int j = 0; j < col; ++j) {
                subMin = ans[nxtPos][j];
                if (j - 1 >= 0) {
                    subMin = min(subMin, ans[nxtPos][j - 1]);
                }
                if (j + 1 < col) {
                    subMin = min(subMin, ans[nxtPos][j + 1]);
                }
                ans[pos][j] = subMin + matrix[i][j];
            }
            pos = nxtPos;
            nxtPos = (pos + 1) % 2;
        }
        int minx = ans[nxtPos][0];
        for (int i = 0; i < col; ++i) minx = min(minx, ans[nxtPos][i]);
        return minx;
    }
};