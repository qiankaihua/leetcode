# Write your MySQL query statement below
SELECT (SELECT `Salary` AS `SecondHighestSalary` 
FROM `Employee` GROUP BY `Salary` ORDER BY `Salary` DESC LIMIT 1, 1) AS `SecondHighestSalary`;