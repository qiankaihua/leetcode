# Minimun Cost to Make at Least One Valid Path in a Grid

[题目链接](https://leetcode.com/problems/minimum-cost-to-make-at-least-one-valid-path-in-a-grid/)

<diff>Hard</diff>

有一个 m * n 的网格，每一个格子都有一个箭头指向下一个你应该访问的格子，格子中1,2,3,4分别代表右左下上。箭头可能指向网格之外。

你最开始在左上角(0,0), 一个合法路径是指从(0,0)到右下角(m - 1, n - 1)，需要按照箭头指示走，但不一定需要是最短的。

你可以花费cost = 1 来改变一个格子的箭头朝向，但是每一个格子你只能改变一次。

返回最小的 cost 使得最小有一条合法路径在网格中。