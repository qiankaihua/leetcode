# Minimum Time to Visit a Cell In a Grid

[题目链接](https://leetcode.com/problems/minimum-time-to-visit-a-cell-in-a-grid)

<diff>Hard</diff>

给一个矩阵, 矩阵中每个数代表这格的解锁时间, 可以走上下左右, 每一步需要 1 单位时间, 问走到右下角最短需要多久? 如果走不到, 返回 -1.