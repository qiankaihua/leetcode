# Majority Element

[题目链接](https://leetcode.com/problems/majority-element/)

<diff>Easy</diff>

给一个数组，求出现次数大于一半的数，保证数组非空并且该数必定存在
