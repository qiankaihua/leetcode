class Solution {
public:
    int minDeletionSize(vector<string>& A) {
        if (A.size() <= 1) return 0;
        vector<bool>bigThenBefore(A.size() - 1, false);
        int len = A[0].length(), size = A.size(), cnt = 0;
        for (int i = 0; i < len; ++i) {
            vector<bool>tmp(size - 1, false);
            bool bk = false;
            for (int j = 1; j < size; ++j) {
                if (!bigThenBefore[j - 1]) {
                    if (A[j - 1][i] == A[j][i]) continue;
                    if (A[j - 1][i] > A[j][i]) {
                        ++cnt;
                        bk = true;
                        break;
                    } else {
                        tmp[j - 1] = true;
                    }
                }
            }
            if (!bk) {
                for (int j = 1; j < size; ++j) {
                    bigThenBefore[j - 1] = bigThenBefore[j - 1] || tmp[j - 1];
                }
            }
        }
        return cnt;
    }
};