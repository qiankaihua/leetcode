class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        unordered_map<int, int> mp;
        for (int i = nums1.size() - 1; i >= 0; --i) {
            if (mp.find(nums1[i]) != mp.end()) {
                mp[nums1[i]]++;
            } else {
                mp[nums1[i]] = 1;
            }
        }
        vector<int>ans;
        for (int i = nums2.size() - 1; i >= 0; --i) {
            if (mp.find(nums2[i]) != mp.end() && mp[nums2[i]] > 0) {
                mp[nums2[i]]--;
                ans.push_back(nums2[i]);
            }
        }
        return ans;
    }
};