# Equal Rational Numbers

[题目链接](https://leetcode.com/problems/equal-rational-numbers/)

<diff>Hard</diff>

给两个字符串, 代表两个数字, 组成可以是 `<integer>.<nonrepeat>(<repeat>)` 每个部分都不必须出现, 除非整数部分为 0, 否则不会有前导 0. 注意 0.9999... 和 1 是相等的
例如:
Input: s = "0.(52)", t = "0.5(25)"
Output: true
Input: s = "0.1666(6)", t = "0.166(66)"
Output: true
Input: s = "0.9(9)", t = "1."
Output: true