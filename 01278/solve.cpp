class Solution {
public:
    int getPre(string &s, int start, int end) {
        int cnt = 0;
        for (int i = start, j = end; i < j; ++i, --j) {
            if (s[i] != s[j]) ++cnt;
        }
        return cnt;
    }
    int palindromePartition(string s, int k) {
        int len = s.length();
        if (k == len) return 0;
        int pre[101][101] = {0};
        int dp[101][101] = {0};
        for (int i = 0; i < len; ++i) {
            for (int j = i; j < len; ++j) {
                pre[i][j] = getPre(s, i, j);
            }
        }
        const int inf = 0x7fffffff;
        for (int i = 0; i < len; ++i) {
            dp[i][0] = pre[0][i];
            for (int j = 1; j < k; ++j) {
                dp[i][j] = inf;
            }
        }
        for (int i = 1; i < k; ++i) {
            for (int j = i; j < len; ++j) {
                for (int t = i - 1; t < j; t++) {
                    dp[j][i] = min(dp[j][i], dp[t][i - 1] + pre[t + 1][j]);
                }
            }
        }
        return dp[len - 1][k - 1];
    }
};