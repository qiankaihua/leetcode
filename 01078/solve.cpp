class Solution {
public:
    vector<string> findOcurrences(string text, string first, string second) {
        string find = first + " " + second + " ";
        vector<string> ans;
        int start = 0, findLen = find.length();
        bool f = true;
        while (start < text.length()) {
            start = text.find(find, start);
            if (f) {
                if (start != 0) {
                    start = 0; f = false; find = " " + find; ++findLen; continue;
                }
            }
            if (start == string::npos || start + findLen >= text.length()) {
                return ans;
            }
            int endd = text.find(' ', start + findLen);
            if (endd == string::npos) endd = text.length();
            ans.emplace_back(text.substr(start + findLen, endd - start - findLen));
            ++start;
            if (f) {f = false, find = " " + find; ++findLen;}
        }
        return ans;
    }
};