# Palindrome Partitioning III

[题目链接](https://leetcode.com/problems/palindrome-partitioning-iii/)

<diff>Hard</diff>

有一个小写字符组成的字符串 s 和一个整数 k ，需要做：

- 首先，将 s 的某些字符改为别的小写字符
- 然后，将 s 划分为 k 个非空的无交集的子串使得每一个子串都是回文串

返回最少需要改变多少个字符