# Maximize Distance to Closest Person

[题目链接](https://leetcode.com/problems/k-similar-strings/)

<diff>Medium</diff>

给一个01数组，0表示座位空，1表示有人，首位不相连，问一个人坐下之后离最近的人最远能有多远
