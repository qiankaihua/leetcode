class Solution {
private:
    int check(vector<int>& ans, int num) {
        long long tmp = 1LL * ans[0] * ans[1];
        if (tmp == num + 1 || tmp == num + 2) return 0;
        return tmp - num - 1;
    }
public:
    vector<int> closestDivisors(int num) {
        vector<int> ans = vector<int>(2);
        ans[0] = floor(sqrt(num) + 1);
        ans[1] = max(ans[0], num / ans[0]);
        int ret;
        while (ans[0] > 0) {
            ret = check(ans, num);
            if (ret == 0) return ans;
            if (ret > 0) {
                --ans[0];
                ans[1] = num / ans[0];
            } else {
                ++ans[1];
            }
        }
        return ans;
    }
};