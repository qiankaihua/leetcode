class Solution {
public:
    bool isRobotBounded(string instructions) {
        int cnt[2] = {0};
        int face = 0;
        int size = instructions.length();
        for (int j = size - 1; j >= 0; --j) {
            switch(instructions[j]) {
                case 'G':
                    cnt[face % 2] += face < 2 ? 1 : -1;
                    break;
                case 'L':
                    face = (face + 1) % 4;
                    break;
                case 'R':
                    face = (face - 1 + 4) % 4;
                    break;
            }
        }
        if (cnt[0] == 0 && cnt[1] == 0) return true;
        if (face != 0) return true;
        return false;
    }
};