/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int maxProduct(TreeNode* root) {
        int max = 0;
        vector<int> v;
        max = travel(root, v);
        int near = 0;
        for (int i = v.size() - 1; i >= 0; --i) {
            if (abs(max - 2 * near) > abs(max - 2 * v[i])) {
                near = v[i];
            }
        }
        return calc(near, max - near);
    }
    int travel(TreeNode* now, vector<int>& v) {
        if (now == NULL) return 0;
        int left = travel(now->left, v);
        int right = travel(now->right, v);
        v.push_back(left);
        v.push_back(right);
        return left + right + now->val;
    }
    int calc(int a, int b) {
        const int mod = 1e9 + 7;
        long long tmp = 0, now;
        stack<int> s;
        while (a) {
            s.push(a % 10);
            a /= 10;
        }
        while (!s.empty()) {
            now = s.top();
            s.pop();
            tmp *= 10;
            tmp += b * now;
            tmp %= mod;
        }
        return tmp;
    }
};