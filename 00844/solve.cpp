class Solution {
public:
    int makeStringReal(string& str) {
        int len = 0;
        for (int i = 0; i < str.length(); ++i) {
            if (str[i] == '#') {
                if (len > 0) --len;
            } else {
                str[len] = str[i];
                ++len;
            }
        }
        return len;
    }
    bool backspaceCompare(string S, string T) {
        int len1 = makeStringReal(S);
        int len2 = makeStringReal(T);
        if (len1 != len2) return false;
        return strncmp(S.c_str(), T.c_str(), len1) == 0;
    }
};