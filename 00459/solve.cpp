class Solution {
public:
    bool repeatedSubstringPattern(string s) {
        int l = s.length(), t = s.length() / 2, i, j;
        for (int len = 1; len <= t; ++len) {
            if (l % len != 0) continue;
            for (i = 0, j = 0; j < l; ++i, ++j) {
                i %= len;
                if (s[j] != s[i]) break;
            }
            if (j == l) return true;
        }
        return false;
    }
};