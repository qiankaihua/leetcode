# K-Similar Strings

[题目链接](https://leetcode.com/problems/k-similar-strings/)

<diff>Hard</diff>

给两个字符串，任意交换第二个字符串的任意两个字符，操作一次为一步，计算最低步数，保证有解。
