# Find K Closest Elements

[题目链接](https://leetcode.com/problems/find-k-closest-elements/)

<diff>Medium</diff>

给你一个数组，和一个目标值，找到和目标值最接近的k个数字，给的数组和返回的数组都是要求有序的
