class Solution {
public:
    bool validateStackSequences(vector<int>& pushed, vector<int>& popped) {
        stack<int> s;
        int pos = 0, tmp;
        for (int i = 0; i < pushed.size(); ++i) {
            s.push(pushed[i]);
            while (!s.empty() && pos < popped.size()) {
                tmp = s.top();
                if (tmp == popped[pos]) {
                    s.pop();
                    ++pos;
                } else {
                    break;
                }
            }
        }
        return s.empty();
    }
};