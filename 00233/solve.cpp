class Solution {
public:
    long long pre[20] = {0};
    int countDigitOne(int n) {
        if (n == 0) return 0;
        pre[1] = 1;
        long long k = 1;
        for (int i = 2; i < 19; ++i) {
            pre[i] = (pre[i - 1] + k) * 10;
            k *= 10;
        }
        int digit[20] = {0};
        int tmp = n;
        int len = 0;
        long long base = 1;
        while (tmp) {
            tmp /= 10;
            len ++;
            base *= 10;
        }
        base /= 10;
        return countRes(n, len, base);
    }
    int countRes(int n, int len, long long base) {
        if (n < 1) return 0;
        if (len == 1) return 1;
        int subCount = pre[len - 1];
        int s = n / base;
        if (s > 1) {
            subCount += base;
            subCount += (s - 1) * pre[len - 1];
        } else {
            subCount += n - base + 1;
        }
        n %= base;
        base /= 10;
        len --;
        while (n < base) {
            base /= 10;
            len--;
        }
        return subCount + countRes(n, len, base);
    }
};