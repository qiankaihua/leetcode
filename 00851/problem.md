# Loud and Rich

[题目链接](https://leetcode.com/problems/loud-and-rich/)

<diff>Medium</diff>

给一组`(x, y)`数对，代表x比y有钱
再给一个数组，quiet，代表每个人的安静值

求出每个人所有不比他穷中的人最安静的人是谁。
