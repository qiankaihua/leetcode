class Solution {
public:
    int maxSumRangeQuery(vector<int>& nums, vector<vector<int>>& requests) {
        sort(nums.begin(), nums.end());
        vector<int> cnt(nums.size() + 10, 0);
        for (auto &v : requests) {
            cnt[v[0]]++;
            cnt[v[1]+1]--;
        }
        for (int i = 1; i < nums.size(); ++i) cnt[i] += cnt[i - 1];
        sort(cnt.begin(), cnt.begin() + nums.size());
        long long sum = 0;
        const long long mod = 1e9+7;
        for (int i = 0; i < nums.size(); ++i) {
            sum = (sum + ((long long)(cnt[i]) * nums[i]) % mod) % mod;
        }
        return sum;
    }
};