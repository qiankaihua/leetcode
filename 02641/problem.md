# Minimum Time to Visit a Cell In a Grid

[题目链接](https://leetcode.com/problems/cousins-in-binary-tree-ii)

<diff>Medium</diff>

给一颗二叉树, 将树的每一个节点的值替换成他的所有表兄弟的值之和.