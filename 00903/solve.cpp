class Solution {
public:
    int numPermsDISequence(string S) {
        int len = S.length();
        const long long mod = 1e9 + 7;
        if (len <= 0) return 0;
        vector<vector<long long>> dp(len + 1, vector<long long>(len + 1, 0));
        dp[0][0] = 1;
        for (int i = 1; i <= len; ++i) {
            for (int j = 0; j <= i; ++j) {
                if (S[i - 1] == 'D') {
                    for (int k = j; k <= i; ++k) {
                        dp[i][j] += dp[i - 1][k];
                        dp[i][j] %= mod;
                    }
                } else {
                    for (int k = 0; k < j; ++k) {
                        dp[i][j] += dp[i - 1][k];
                        dp[i][j] %= mod;
                    }
                }
            }
        }
        long long ans = 0;
        for (int i = 0; i <= len; ++i) {
            ans += dp[len][i];
            ans %= mod;
        }
        return ans;
    }
};