class Solution {
public:
    bool isSub(vector<string>& sub, vector<string>& all) {
        if (sub.size() > all.size()) return false;
        int i = 0;
        for (int j = 0; i < sub.size() && j < all.size(); ++j) {
            if (sub[i] == all[j]) ++i;
        }
        return i == sub.size();
    }
    vector<int> peopleIndexes(vector<vector<string>>& favoriteCompanies) {
        vector<int> ans;
        for (int i = 0; i < favoriteCompanies.size(); ++i) {
            sort(favoriteCompanies[i].begin(), favoriteCompanies[i].end());
        }
        for (int i = 0; i < favoriteCompanies.size(); ++i) {
            bool flag = true;
            for (int j = 0; j < favoriteCompanies.size(); ++j) {
                if (i == j) continue;
                if (isSub(favoriteCompanies[i], favoriteCompanies[j])) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                ans.emplace_back(i);
            }
        }
        return ans;
    }
};