# Search a 2D Matrix

[题目链接](https://leetcode.com/problems/search-a-2d-matrix/)

<diff>Medium</diff>

给一个二维数组，每一行从小到大排序，每一行第一个都比上一行最后一个大，给一个数，找到这个数是不是在这个数组里面。

二维数组长宽不大于100，值得绝对值小于10^4