实现LFU比较简单，用一个双向链表保存cache的访问顺序和次数的排序，一个map保存key对应的节点即可。
但是为了实现cache的所有操作都是O(1)的，必须修改算法。

我的办法是，用一个vector代替map保存对应的节点，这个可以用空间来换取时间，把查询节点的操作复杂度降低为O(1)，然后再建立一个vector来保存每一个访问次数的最后访问的节点，这个的话能在O(1)的时间复杂度之内找到更新的节点所处的位置，这样将更新操作的复杂度也降低到了O(1)。

但是这个办法是用大量的空间换取时间，而且建立在题目的数据范围key<=1e4，总次数<=1e5，因为限制了总次数所以才能用vector来O(1)访问，否则空间会爆炸，就算在这个范围下，空间也使用到了62M，感觉会有别的做法。

下面是在leetcode上面比较快的，而且又只用了40M内存的代码，找时间研究一下。（研究了之后才知道unordered_map的操作复杂度是O(1)的，那用这个处理unordered_map处理就方便很多了

```cpp
class LFUCache {
public:
    class Node {
    public:
        Node(int k = 0, int v = 0) {
            key = k;
            val = v;
            freq = 1;
            prev = next = NULL;
        }
        int key;
        int val;
        int freq;
        Node *prev, *next;
    };
    
    class DLinkedList {
    public:
        DLinkedList(){
            dummy = new Node();
            dummy->prev = dummy->next = dummy;
            size = 0;
        }
        Node *dummy;
        int size;
        
/*
    append(node): append the node to the head of the linked list.
    pop(node=None): remove the referenced node. 
                    If None is given, remove the one from tail, which is the least recently used.
                    
    Both operation, apparently, are in O(1) complexity.
  */      
        void append(Node *node) {
            if (node == NULL)
                return;
            node->next = dummy->next;
            node->prev = dummy;
            if (node->next != NULL)
                node->next->prev = node;
            dummy->next = node;
            size += 1;
        }
        
        Node* pop(Node *node = NULL) {
            if (size == 0)
                return NULL;
            if (node == NULL)
                node = dummy->prev;
            
            if (node != NULL) {
                if (node->prev != NULL)
                    node->prev->next = node->next;
                if (node->next != NULL)
                    node->next->prev = node->prev;
                size -= 1;
            }
            
            return node;
        }
    };
    
    LFUCache(int capacity) {
        _size = 0;
        _capacity = capacity;
        _minFreq = 0;
    }
    
    /*
            This is a helper function that used in the following two cases:
        
            1. when `get(key)` is called; and
            2. when `put(key, value)` is called and the key exists.
         
        The common point of these two cases is that:
        
            1. no new node comes in, and
            2. the node is visited one more times -> node.freq changed -> 
               thus the place of this node will change
        
        The logic of this function is:
        
            1. pop the node from the old DLinkedList (with freq `f`)
            2. append the node to new DLinkedList (with freq `f+1`)
            3. if old DlinkedList has size 0 and self._minfreq is `f`,
               update self._minfreq to `f+1`
    */
    
    void update(Node *node) {
        int freq = node->freq;
        _freqMap[freq].pop(node);

        if (_minFreq == freq && _freqMap[freq].size == 0)
            _minFreq += 1;
                
        node->freq += 1;
        freq = node->freq;
        _freqMap[freq].append(node);
    }
    
    /*
        Through checking self._node[key], we can get the node in O(1) time.
        Just performs self._update, then we can return the value of node.
    */
    
    int get(int key) {
        if (!_keyMap.count(key))
            return -1;
        Node *node = _keyMap[key];
        update(node);
        return node->val;
    }
    
    /*
        If `key` already exists in self._node, we do the same operations as `get`, except
        updating the node.val to new value.
        
        Otherwise, the following logic will be performed
        
        1. if the cache reaches its capacity, pop the least frequently used item. (*)
        2. add new node to self._node
        3. add new node to the DLinkedList with frequency 1
        4. reset self._minfreq to 1
        
        (*) How to pop the least frequently used item? Two facts:
        
        1. we maintain the self._minfreq, the minimum possible frequency in cache.
        2. All cache with the same frequency are stored as a DLinkedList, with
           recently used order (Always append at head)
          
        Consequence? ==> The tail of the DLinkedList with self._minfreq is the least
                         recently used one, pop it...
    */
    
    void put(int key, int value) {
        if (_capacity == 0)
            return;
        Node *node = NULL;
        if (_keyMap.count(key)) {
            node = _keyMap[key];
            update(node);
            node->val = value;
        } else {
            if (_size == _capacity) {
                node = _freqMap[_minFreq].pop();
                if (node != NULL) {
                    _keyMap.erase(node->key);
                }
                _size -= 1;
            }
            node = new Node(key, value);
            _keyMap.insert({key, node});
            _freqMap[1].append(node);
            _minFreq = 1;
            _size += 1;
        }
    }
private:
    unordered_map<int, Node*> _keyMap;
    unordered_map<int, DLinkedList> _freqMap;
    int _size;
    int _capacity;
    int _minFreq;
};

/**
 * Your LFUCache object will be instantiated and called as such:
 * LFUCache* obj = new LFUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */

```