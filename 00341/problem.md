# Flatten Nested List Iterator

[题目链接](https://leetcode.com/problems/flatten-nested-list-iterator/)

<diff>Medium</diff>

给你一个 list， 每一个元素可能是list或者一个数字，list里的元素也是同理

现在让你迭代输出这个list中的每一个数字。

例如：

input: `[[1,1],2,[1,1]]`
output: `[1,1,2,1,1]`
