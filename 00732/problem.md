# My Calendar III

[题目链接](https://leetcode.com/problems/my-calendar-iii/)

<diff>Hard</diff>

让你构造一个日历类，有一个book函数，输入事件的起始时间，返回同一时间最多同时存在几个事件。

时间范围为左闭右开`[start, end)`，最大10e9

最多操作400次
