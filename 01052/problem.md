# Grumpy Bookstore Owner

[题目链接](https://leetcode.com/problems/grumpy-bookstore-owner/submissions/)

<diff>Medium</diff>

给一个customers数组，代表每个时间点的顾客，一个grumpy数组，代表店长生气的时间，为1时生气，店长有一个技巧能在X的连续时间内保持不生气，生气的时候顾客会不高兴，问最多能让多少顾客满意。
