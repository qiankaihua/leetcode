# Shortest Path to Get All Keys

[题目链接](https://leetcode.com/problems/shortest-path-to-get-all-keys/)

<diff>Hard</diff>

给一个地图，`@`代表起点，`#`代表墙，`.`点可以走，`a-f`代表钥匙，`A-F`代表锁，问最少几步可以拿到所以钥匙。