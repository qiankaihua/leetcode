# Score After Flipping Matrix

[题目链接](https://leetcode.com/problems/score-after-flipping-matrix/)

<diff>Medium</diff>

给一个01矩阵，你可以翻转一行或者一列的01，然后把每行看成一个二进制数，计算所有行加起来的总和，求出最大的和。
