/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isValidBST(TreeNode* root) {
        int64_t last = INT64_MIN;
        return travel(root, last);
    }
    bool travel(TreeNode* root, int64_t &last) {
        if (root == nullptr) return true;
        if (root->left != nullptr) {
            if (!travel(root->left, last)) {
                return false;
            }
        }
        if (root->val <= last) return false;
        last = root->val;
        return travel(root->right, last);
    }

};