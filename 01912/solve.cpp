class MovieRentingSystem {
public:
    MovieRentingSystem(int n, vector<vector<int>>& entries) {
        for (auto v : entries) {
            vector<int> t;
            t.push_back(v[2]);
            t.push_back(v[0]);
            t.push_back(v[1]);
            movie2shop[v[1]].insert(t);
            shop2movie2price[v[0]][v[1]] = v[2];
        }
    }
    
    // 查最便宜的5个商店没有租出去的
    vector<int> search(int movie) {
        vector<int> ans;
        int size = 0;
        for (auto v : movie2shop[movie]) {
            ans.push_back(v[1]);
            ++size;
            if (size == 5) {
                break;
            }
        }
        return ans;
    }
    
    // 租指定的影片
    void rent(int shop, int movie) {
        auto price = shop2movie2price[shop][movie];
        vector<int> t;
        t.push_back(price);
        t.push_back(shop);
        t.push_back(movie);
        rented.insert(t);
        
        vector<int> t2;
        t2.push_back(price);
        t2.push_back(shop);
        t2.push_back(movie);
        movie2shop[movie].erase(t2);
    }
    
    // 归还指定影片
    void drop(int shop, int movie) {
        auto price = shop2movie2price[shop][movie];
        vector<int> t;
        t.push_back(price);
        t.push_back(shop);
        t.push_back(movie);
        rented.erase(t);
        
        vector<int> t2;
        t2.push_back(price);
        t2.push_back(shop);
        t2.push_back(movie);
        movie2shop[movie].insert(t2);
    }
    
    // 返回最便宜的5个租出去的影片，如果价格一样，s_id小的在前面，还一样m_id小的在前面
    vector<vector<int>> report() {
        vector<vector<int>> ans;
        int size = 0;
        for (auto r : rented) {
            vector<int> t;
            t.push_back(r[1]);
            t.push_back(r[2]);
            ans.push_back(t);
            ++size;
            if (size == 5) {
                break;
            }
        }
        return ans;
    }
private:
    map<int, set<vector<int>>> movie2shop; // <price, sid, mid>
    set<vector<int>> rented; // <price, sid, mid>
    map<int, map<int, int>> shop2movie2price;
};

/**
 * Your MovieRentingSystem object will be instantiated and called as such:
 * MovieRentingSystem* obj = new MovieRentingSystem(n, entries);
 * vector<int> param_1 = obj->search(movie);
 * obj->rent(shop,movie);
 * obj->drop(shop,movie);
 * vector<vector<int>> param_4 = obj->report();
 */