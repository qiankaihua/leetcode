class Solution {
public:
    vector<bool> prefixesDivBy5(vector<int>& A) {
        vector<bool> ans(A.size(), false);
        int tmp = 0;
        for (int i = 0; i < A.size(); ++i) {
            tmp = (tmp << 1) + A[i];
            tmp %= 5;
            if (tmp == 0) {
                ans[i] = true;
            }
        }
        return ans;
    }
};