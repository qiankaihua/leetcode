# Set Matrix Zeroes

[题目链接](https://leetcode.com/problems/set-matrix-zeroes)

<diff>Medium</diff>

给一个矩阵, 将所有为 0 的点该行该列都改为 0.