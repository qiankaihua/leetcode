# Number of Music Playlists

[题目链接](https://leetcode.com/problems/number-of-music-playlists/)

<diff>Hard</diff>

你有N首歌，你需要构造一个长度为L的音乐播放列表，使得每一首歌都在播放列表中，并且相同的歌中间必须间隔K首歌，问有多少种播放列表构造方法，答案MOD 1e9 + 7
