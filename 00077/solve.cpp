class Solution {
public:
    vector<vector<int>> combine(int n, int k) {
        vector<vector<int>> ans;
        if (n == 0 || k == 0) return ans;
        vector<int> tmp(k);
        dfs(ans, tmp, n, k, 1, 0);
        return ans;
    }
    void dfs(vector<vector<int>> &ans, vector<int> &tmp, int n, int k, int now, int pick) {
        if (now > n) return;
        tmp[pick] = now;
        if (pick + 1 == k) {
            ans.emplace_back(tmp);
        } else {
            dfs(ans, tmp, n, k, now + 1, pick + 1);
        }
        dfs(ans, tmp, n, k, now + 1, pick);
    }
};