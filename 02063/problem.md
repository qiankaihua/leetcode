# Vowels of All Substrings

[题目链接](https://leetcode.com/problems/vowels-of-all-substrings/)

<diff>Medium</diff>

给一个字符串，统计这个字符串所有子串包含的元音数量，个人觉得不值得中等难度。
