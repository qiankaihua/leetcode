DID

0123

0：
1：0
2: 10
3：102,201
4: 1032,2031,3021,2130,3120

不计算超过的
1 0 0 0 d
1 0 0 0 i
0 1 1 0 d
2 2 1 0

多模拟几个，可以发现一个规律，在末尾添加一个数字，能添加什么之和最后一个数字有关

例如，结尾为2，如果是D，那么末尾可以添加0,1,2，就有三种，虽然加入的数字是4，但是我们可以这么做，如果末尾添加的是2，则可以让原串所有大于等于2的数全部加一，就可构成新串了。如果是I则可以推出末尾可以添加3,4.

稍微转换一下可以得到dp方程：

- if `s[i - 1] = 'D'` `dp[i][j] = sum(dp[i - 1][k]) (j <= k <= i)`
- if `s[i - 1] = 'I'` `dp[i][j] = sum(dp[i - 1][k]) (0 <= k < j)`

最后答案是 `sum[len(s)][i] (0 <= i <= len(s))`
