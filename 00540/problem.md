# Single Element in a Sorted Array

[题目链接](https://leetcode.com/problems/single-element-in-a-sorted-array/)

<diff>Medium</diff>

给一个数组，其中只有一个数字只出现一次，其余的都恰好出现两次。找出那个只出现一次的数

时间复杂度不超过log(n)，空间复杂度不超过1
