/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* reverseBetween(ListNode* head, int m, int n) {
        ListNode *now, *begin, *nxt, *nntx, *header = new ListNode(-1, head);
        int i = 0;
        begin = header;
        while (++i < m) begin = begin->next;;
        now = begin->next;
        nxt = now->next;
        for (; i < n; ++i) {
            nntx = nxt->next;
            nxt->next = now;
            
            now = nxt;
            nxt = nntx;
        }
        begin->next->next = nxt;
        begin->next = now;
        return header->next;
    }
};