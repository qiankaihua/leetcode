# Subarray Product Less Than K

[题目链接](https://leetcode.com/problems/subarray-product-less-than-k/)

<diff>Medium</diff>

给一个数组的和一个 k ，求严格小于 k 的`子数组`有多少个

> 是子数组而不是子序列，要求连续

数字范围 `[1, 1000]`
数字个数 `[1, 30000]`
k的范围  `[0, 1000000]`