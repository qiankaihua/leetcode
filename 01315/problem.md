# Sum of Nodes with Even-Valued Grandparent

[题目链接](https://leetcode.com/problems/sum-of-nodes-with-even-valued-grandparent/)

<diff>Medium</diff>

给你一颗树，求所有祖父节点值是偶数的节点之和
