# Maximum Number of Achievable Transfer Requests

[题目链接](https://leetcode.com/problems/maximum-number-of-achievable-transfer-requests)

<diff>Hard</diff>

有 n 个公司,每个公司都是满员的,也就是跳槽必须跳出去多少才能跳进来多少,给一个跳槽请求列表,第一位代表从哪家公司跳出来,第二位代表跳到哪家公司,问最多能满足多少人的跳槽请求

n <= 20
跳槽请求数量 <= 16
