struct Node {
    int x, y;
};
int operator<(const Node a, const Node b) {
    return a.x < b.x;
}

class Solution {
public:
    int minTaps(int n, vector<int>& ranges) {
        int cnt = 0, maxPos = 0, nowPos = 0, start;
        vector<Node> r(n + 1);
        for (int i = 0; i <= n; ++i) {
            r[i] = Node{i - ranges[i], i + ranges[i]};
        }
        sort(r.begin(), r.end());
        for (int i = 0; i <= n;) {
            maxPos = 0;
            start = i;
            for (; i <= n && r[i].x <= nowPos; ++i) {
                maxPos = max(r[i].y, maxPos);
            }
            if (start == i) return -1;
            ++cnt;
            nowPos = maxPos;
            if (nowPos >= n) break;
        }
        if (nowPos < n) return -1;
        return cnt;
    }
};