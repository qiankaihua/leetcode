/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* bstToGst(TreeNode* root) {
        if (root == nullptr) {
            return root;
        }
        int sum = 0;
        doHandle(root, &sum);
        return root;
    }
    void doHandle(TreeNode *now, int* sum) {
        if (now->right != nullptr) {
            doHandle(now->right, sum);
        }
        *sum += now->val;
        now->val = *sum;
        if (now->left != nullptr) {
            doHandle(now->left, sum);
        }
    }
    // 从最大开始从小到大遍历二叉树，然后一路累加
};