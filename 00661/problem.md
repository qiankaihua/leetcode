# Open the Lock

[题目链接](https://leetcode.com/problems/image-smoother/)

<diff>Easy</diff>

给一个二维数组，求出每一个元素加上周围最多9个格子的平均值，向下取整。