class Solution {
public:
    unordered_map<int, int> mp;
    Solution() {
        mp[1] = 1;
        mp[0] = 0;
    }
    int minDays(int n) {
        if (mp.find(n) != mp.end()) return mp[n];
        int ans = 1 + min(minDays(n/2)+n%2, minDays(n/3)+n%3);
        mp[n] = ans;
        return ans;
    }
};