impl Solution {
    pub fn trap(height: Vec<i32>) -> i32 {
        let mut left = vec![0; height.len()];
        let mut right = vec![0; height.len()];
        left[0] = height[0];
        for i in 1..height.len() {
            left[i] = left[i - 1].max(height[i]);
        }
        right[height.len() - 1] = height[height.len() - 1];
        for i in (0..height.len() - 1).rev() {
            right[i] = right[i + 1].max(height[i]);
        }
        let mut sum = 0;
        for i in 1..height.len() - 1 {
            sum += 0.max(left[i - 1].min(right[i + 1]) - height[i]);
        }
        return sum;
    }
}
