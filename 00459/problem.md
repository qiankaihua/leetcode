# Repeated Substring Pattern

[题目链接](https://leetcode.com/problems/repeated-substring-pattern/)

<diff>Easy</diff>

给你一个字符串，判断它是不是由重复子串构成的，例如 "abab"
