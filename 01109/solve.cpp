// 691ms
bool sortFunction(vector<int>& a, vector<int>& b) {
    return a[0] < b[0];
}
bool sortBackFunction(vector<int>& a, vector<int>& b) {
    return a[1] < b[1];
}
class Solution {
public:
    vector<int> corpFlightBookings(vector<vector<int>>& bookings, int n) {
        vector<vector<int>> bookings_end(bookings);
        sort(bookings.begin(), bookings.end(), sortFunction);
        sort(bookings_end.begin(), bookings_end.end(),sortBackFunction);
        vector<int> ans(n, 0);
        int now = 0;
        int add = 0, remove = 0;
        for (int i = 0; i < n; ++i) {
            while (add < bookings.size() && bookings[add][0] <= i + 1) {
                now += bookings[add][2];
                ++add;
            }
            while (remove < bookings_end.size() && bookings_end[remove][1] < i + 1) {
                now -= bookings_end[remove][2];
                ++remove;
            }
            ans[i] = now;
        }
        return ans;
    }
};
// 单数组不排序 184ms
class Solution {
public:
    vector<int> corpFlightBookings(vector<vector<int>>& arr, int n) {
        
        vector<int> ans(n+1,0);
        int m=arr.size();
        for(int i=0;i<m;i++)
        {
            ans[arr[i][0]]+=arr[i][2];
            if(arr[i][1]+1<=n)
            ans[arr[i][1]+1]-=arr[i][2];
        }
        
        for(int i=2;i<=n;i++)
        {
            ans[i]+=ans[i-1];
        }
        ans.erase(ans.begin());
        return ans;
    }
};