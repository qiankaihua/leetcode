class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int n = matrix.size();
        if (n == 0) return false;
        int m = matrix[0].size();
        if (m == 0) return false;
        int l = 0, r = m * n, mid;
        while (l < r) {
            mid = l + (r - l) / 2;
            //cout << l << ' ' << r << ' ' << mid << endl;
            //cout << mid / m << ' ' << mid % m << endl;
            if (matrix[mid / m][mid % m] > target) {
                r = mid;
            } else if (matrix[mid / m][mid % m] == target) {
                return true;
            } else {
                l = mid + 1;
            }
        }
        return false;
    }
};