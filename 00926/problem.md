# Flip String to Monotone Increasing

[题目链接](https://leetcode.com/problems/flip-string-to-monotone-increasing/)

<diff>Medium</diff>

给一个包含0,1的字符串，让你变换成(0\*1\*)的形式(正则表达)，0->1或者1->0算一次操作，问最少几次操作
