# LFU Cache

[题目链接](https://leetcode.com/problems/lfu-cache/)

<diff>Hard</diff>

实现一个LFU cache(Least Frequently Used 移除最不频繁访问的节点，如果频率相同，移除最早访问的节点)