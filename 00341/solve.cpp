/**
 * // This is the interface that allows for creating nested lists.
 * // You should not implement it, or speculate about its implementation
 * class NestedInteger {
 *   public:
 *     // Return true if this NestedInteger holds a single integer, rather than a nested list.
 *     bool isInteger() const;
 *
 *     // Return the single integer that this NestedInteger holds, if it holds a single integer
 *     // The result is undefined if this NestedInteger holds a nested list
 *     int getInteger() const;
 *
 *     // Return the nested list that this NestedInteger holds, if it holds a nested list
 *     // The result is undefined if this NestedInteger holds a single integer
 *     const vector<NestedInteger> &getList() const;
 * };
 */

class NestedIterator {
public:
    queue<int> q;
    NestedIterator(vector<NestedInteger> &nestedList) {
        while(!q.empty()) q.pop();
        for (int i = 0; i < nestedList.size(); ++i) {
            dfs(nestedList[i]);
        }
    }
    
    int next() {
        int tmp = q.front();
        q.pop();
        return tmp;
    }
    
    bool hasNext() {
        return !q.empty();
    }
private:
    void dfs(NestedInteger now) {
        if (now.isInteger()) {
            q.push(now.getInteger());
            return;
        }
        vector<NestedInteger> tmp = now.getList();
        for (int i = 0; i < tmp.size(); i++) {
            dfs(tmp[i]);
        }
    }
};

/**
 * Your NestedIterator object will be instantiated and called as such:
 * NestedIterator i(nestedList);
 * while (i.hasNext()) cout << i.next();
 */