#!/bin/bash

# 接受参数
input=$1

# 将输入参数解释为十进制数
input_decimal=$((10#$input))


# 格式化为五位数并补充前导零
output=$(printf "%05d" $input_decimal)

if [ ! -d "$output" ]; then
    echo "not found $output"
else
    echo "found $output"
fi