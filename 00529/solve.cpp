class Solution {
public:
    int mx[8] = {-1, -1, -1, 0, 0, 1, 1, 1};
    int my[8] = {-1, 0, 1, -1, 1, -1, 0, 1};
    int isMine(vector<vector<char>>& board, int x, int y) {
        if (x < 0 || x >= board.size() || y < 0 || y >= board[0].size()) return 0;
        return board[x][y] == 'M';
    }
    int getMine(vector<vector<char>>& board, int x, int y) {
        int ans = 0;
        for (int i = 0; i < 8; ++i) {
            ans += isMine(board, x + mx[i], y + my[i]);
        }
        return ans;
    }
    bool checkPos(vector<vector<char>>& board, int x, int y) {
        if (x < 0 || x >= board.size() || y < 0 || y >= board[0].size()) return false;
        return board[x][y] == 'E';
    }
    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click) {
        if (board[click[0]][click[1]] == 'M') {
            board[click[0]][click[1]] = 'X';
            return board;
        }
        if (board[click[0]][click[1]] == 'E') {
            int n = getMine(board, click[0], click[1]);
            if (n != 0) {
                board[click[0]][click[1]] = '0' + n;
                return board;
            }
            board[click[0]][click[1]] = 'B';
            vector<int> nxt(2);
            for (int i = 0; i < 8; ++i) {
                if (checkPos(board, click[0] + mx[i], click[1] + my[i])) {
                    nxt[0] = click[0] + mx[i];
                    nxt[1] = click[1] + my[i];
                    updateBoard(board, nxt);
                }
            }
            return board;
        }
        return board;
    }
};