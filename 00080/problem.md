# Remove Duplicates from Sorted Array II

[题目链接](https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii)

<diff>Medium</diff>

在一个有序列表中原地移除出现超过两次的重复数
例如: [1,2,3,4,4,4,5,5] => [1,2,3,4,4,5,5]
