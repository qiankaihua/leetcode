class Trie {
    struct Node {
        vector<Node*> nxt;
        bool hasEnd;
        Node() {
            nxt = vector<Node*>(26, NULL);
            hasEnd = false;
        }
    };
    Node* start;
    Node* last;
public:
    Trie() {
        start = new Node;
    }
    bool search(vector<char> &v) {
        Node *now = start;
        for (int i = v.size() - 1; i >= 0; --i) {
            if (now->nxt[v[i] - 'a'] == NULL) return false;
            now = now->nxt[v[i] - 'a'];
            if (now->hasEnd) return true;
        }
        return false;
    }
    void insert(string &s) {
        Node *now = start;
        for (int i = s.length() - 1; i >= 0; --i) {
            if (now->nxt[s[i] - 'a'] == NULL) {
                now->nxt[s[i] - 'a'] = new Node;
            }
            now = now->nxt[s[i] - 'a'];
        }
        now->hasEnd = true;
    }
};
class StreamChecker {
public:
    Trie* tree;
    vector<char> query_chars;
    StreamChecker(vector<string>& words) {
        tree = new Trie();
        for (int i = words.size() - 1; i >= 0; --i) {
            tree->insert(words[i]);
        }
    }
    
    bool query(char letter) {
        query_chars.push_back(letter);
        return tree->search(query_chars);
    }
};

/**
 * Your StreamChecker object will be instantiated and called as such:
 * StreamChecker* obj = new StreamChecker(words);
 * bool param_1 = obj->query(letter);
 */