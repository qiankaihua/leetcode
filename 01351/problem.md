# Count Negative Numbers in a Sorted Matrix

[题目链接](https://leetcode.com/problems/count-negative-numbers-in-a-sorted-matrix/)

<diff>Easy</diff>

有一个 m * n 的网格，从上到下从左到右不上升（<=），求负数的个数
