# Single Number

[题目链接](https://leetcode.com/problems/single-number/)

<diff>Easy</diff>

给一个数组，其中只有一个数字只出现了一次，其他都恰好出现了两次，找出只出现一次的数
