class Solution {
public:
    int balancedStringSplit(string s) {
        if (s.length() == 0) return 0;
        int cnt = s[s.length() - 1] == 'R' ? 1 : -1;
        int ans = 0;
        for (int i = s.length() - 2; i >= 0; --i) {
            cnt += (s[i] == 'R' ? 1 : -1);
            if (cnt == 0) ++ans;
        }
        return ans;
    }
};