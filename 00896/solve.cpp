class Solution {
public:
    bool isMonotonic(vector<int>& A) {
        if (A.size() < 3) return true;
        bool canJudge = false, isDown;
        for (int i = 1; i < A.size(); ++i) {
            if (canJudge) {
                if (isDown && A[i - 1] < A[i]) return false;
                if (!isDown && A[i - 1] > A[i]) return false;
            } else {
                if (A[i - 1] < A[i]) {
                    canJudge = true;
                    isDown = false;
                }
                if (A[i - 1] > A[i]) {
                    canJudge = true;
                    isDown = true;
                }
            }
        }
        return true;
    }
};