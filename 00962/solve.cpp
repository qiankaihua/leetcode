// search
class Solution {
public:
    int maxWidthRamp(vector<int>& nums) {
        vector<int> pre(nums.size());
        int minn = 9999999;
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] < minn) {
                minn = nums[i];
            }
            pre[i] = -minn;
        }
        int maxx = 0, ans = 0;
        for (int i = nums.size() - 1; i >= 0; --i) {
            ans = max(ans, i - getMinPos(pre, nums[i]));
        }
        return ans;
    }
    int getMinPos(vector<int>& v, int val) {
        auto p = lower_bound(v.begin(), v.end(), -val);
        if (p == v.end()) return -1;
        return p - v.begin();
    }
};
// stack
class Solution {
public:
    int maxWidthRamp(vector<int>& nums) {
        stack<int> s;
        int minn = 9999999;
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] < minn) {
                minn = nums[i];
                s.push(i);
            }
        }
        int ans = 0;
        for (int i = nums.size() - 1; i >= 0; --i) {
            while (!s.empty() && nums[s.top()] <= nums[i]) {
                ans = max(ans, i - s.top());
                s.pop();
            }
        }
        return ans;
    }
};