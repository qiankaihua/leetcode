struct node {
    int score, index;
    node() {
        score = 0, index = 0;
    }
};
int cmp(node a, node b) {
    return a.score > b.score;
}
class Solution {
public:
    string getAns(int index) {
        switch (index) {
            case 0:
                return "Gold Medal";
            case 1:
                return "Silver Medal";
            case 2:
                return "Bronze Medal";
            default:
                return to_string(index + 1);
        }
    }
    vector<string> findRelativeRanks(vector<int>& nums) {
        vector<string>ans(nums.size());
        vector<node>tmp(nums.size());
        for (int i = 0; i < nums.size(); ++i) {
            tmp[i].score = nums[i];
            tmp[i].index = i;
        }
        sort(tmp.begin(), tmp.end(), cmp);
        for (int i = 0; i < nums.size(); ++i) {
            ans[tmp[i].index] = getAns(i);
        }
        return ans;
    }
};