class Solution {
public:
    int findMaxLength(vector<int>& nums) {
        unordered_map<int, int> mp;
        int cnt = 0, ans = 0;
        mp[0] = -1;
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] == 0) ++cnt;
            else --cnt;
            if (mp.find(cnt) == mp.end()) {
                mp[cnt] = i;
            } else {
                ans = max(ans, i - mp[cnt]);
            }
        }
        return ans;
    }
};