第一次做数位dp的题，刚开始还是看了下题解的。
其实类似记忆化搜索，记录`dp_[当前位数][当前奇数位数][当前偶数位数][当前模k的余数][是否处于边界]`，其他都简单，重点是最后一位，例如 `l` 是 `123456`，枚举到第三位的时候，`123` 就是处于边界情况，`120` 就不处于边界，处不处于边界情况时处理逻辑会不同，比如当前位可以枚举的数字就不一样，这个感觉就是和普通的dp最大的区别

第二种方法是优化后的暴力，累加k，然后判断奇偶位数是否符合条件，虽然条件的l，r小于1e9，但是由于要满足奇偶位数相等的条件，所以1e8-1e9中的数据是不可能满足条件的，所以可以暴力过。
https://leetcode.com/problems/number-of-beautiful-integers-in-the-range/solutions/3936564/simple-and-small-code-without-dp-space-complexity-o-1-completely-intuitive