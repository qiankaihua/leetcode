class Solution {
public:
    int find_st(vector<int>& st, int n) {
        return st[n] == n ? n : st[n] = find_st(st, st[n]);
    }
    vector<int> findRedundantConnection(vector<vector<int>>& edges) {
        vector<int> st(edges.size() + 1);
        for (int i = 1; i <= edges.size(); ++i) st[i] = i;
        int rt, rt2;
        for (auto e : edges) {
            rt = find_st(st, e[1]);
            rt2 = find_st(st, e[0]);
            if (rt == rt2) return e;
            if (rt > rt2) st[rt] = rt2;
            else st[rt2] = rt;
        }
        return {0, 0};
    }
};