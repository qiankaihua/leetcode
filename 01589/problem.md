# Maximum Sum Obtained of Any Permutation

[题目链接](https://leetcode.com/problems/maximum-sum-obtained-of-any-permutation/)

<diff>Medium</diff>

给一个数组，有n个数，再给一个请求列表，每一个请求是`[start,end]`，表示对于第一个数组对应下标范围求和一次，现在重排列第一个数据，求出和的最大值。

最终答案对于1e9+7取模
