class Solution {
public:
    vector<int>sumArr;
    int sum;
    Solution(vector<int>& w) {
        int s = 0;
        for (int i = 0; i < w.size(); ++i) {
            s += w[i];
            sumArr.push_back(s);
        }
        sum = s;
        srand(time(NULL));
    }
    
    int pickIndex() {
        return findIndexForRandom(rand() % sum);
    }
private:
    int findIndexForRandom(int random) {
        int l = 0, r = sumArr.size() - 1, mid;
        while (l < r) {
            mid = l + (r - l) / 2;
            if (random < sumArr[mid]) r = mid;
            else l = mid + 1;
        }
        return l;
    }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(w);
 * int param_1 = obj->pickIndex();
 */