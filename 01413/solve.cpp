class Solution {
public:
    int minStartValue(vector<int>& nums) {
        if (nums.size() == 0) return 1;
        int ans = 1, tmp = 0;
        for (int i = 0; i < nums.size(); ++i) {
            tmp += nums[i];
            ans = max(ans, 1 - tmp);
        }
        return ans;
    }
};