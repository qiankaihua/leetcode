#ifdef LEETCODE_INCLUDE_TEST
#include "../0test/include.h"
#endif

class Solution {
public:
    int maximumUniqueSubarray(vector<int>& nums) {
        int now = 0, ans = 0;
        bool flag[10005] = {false};
        int j = -1;
        for (int i = 0; i < nums.size(); ++i) {
            if (!flag[nums[i]]) {
                flag[nums[i]] = true;
                now += nums[i];
            } else {
                for (; j < i && nums[j+1] != nums[i]; ++j) {
                    flag[nums[j + 1]] = false;
                    now -= nums[j + 1];
                }
                ++j;
            }
            ans = max(ans, now);
        }
        ans = max(ans, now);
        return ans;
    }
};