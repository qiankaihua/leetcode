class Solution {
public:
    int findMaxValueOfEquation(vector<vector<int>>& points, int k) {
        int now_max1 = INT_MIN, now_max2 = INT_MIN, nowX, ans = INT_MIN;
        if (points.size() == 0) return 0;
        vector<pair<int, int>> tmp;
        nowX = points[0][0];
        now_max1 = points[0][1];
        for (int i = 1; i < points.size(); ++i) {
            if (points[i][0] != nowX) {
                tmp.emplace_back(pair<int, int>(nowX, now_max1));
                nowX = points[i][0];
                now_max1 = points[i][1];
                now_max2 = INT_MIN;
            } else {
                if (now_max1 < points[i][1]) {
                    now_max2 = now_max1;
                    now_max1 = points[i][1];
                } else if (now_max2 < points[i][1]) {
                    now_max2 = points[i][1];
                }
                ans = max(ans, now_max1 + now_max2);
            }
        }
        tmp.emplace_back(pair<int, int>(nowX, now_max1));
        
        int l = 0, r = 1, disX, dd;
        deque<int> q;
        q.push_back(tmp[0].second - tmp[0].first);
        while (l < r && r < tmp.size()) {
            while (r - l < q.size()) {
                q.pop_front();
            }
            disX = tmp[r].first - tmp[l].first;
            if (disX <= k) {
                ans = max(ans, q.front() + tmp[r].second + tmp[r].first);
            }
            if (r + 1 < tmp.size()) {
                if (l + 1 >= r || tmp[r + 1].first - tmp[l].first <= k) {
                    dd = tmp[r].second - tmp[r].first;
                    while (q.size() > 0 && dd >= q.front()) {
                        q.pop_front();
                    }
                    q.push_back(dd);
                    ++r;
                } else {
                    ++l;
                }
            } else {
                ++l;
            }
        }
        return ans;
    }
};