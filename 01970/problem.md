# Last Day Where You Can Still Cross

[题目链接](https://leetcode.com/problems/last-day-where-you-can-still-cross/)

<diff>Hard</diff>

给一个 row 行 col 列的矩阵
再给一个长度为 row * col 的数组，数组中每一项代表一个格子 `<x, y>` （下标从 1 开始），会在指定格子上放置障碍物，问最多放置几个障碍物之后还能从矩阵的上边界进入，然后从下边界出来
