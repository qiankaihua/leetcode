# Detect Capital

[题目链接](https://leetcode.com/problems/detect-capital/)

<diff>Easy</diff>

判断一个单词是否合法，合法的单词种类如下：

1. 所有的字母都是大写的
2. 所有的字母都是小写的
3. 只有首字母是大写的，剩下都是小写的