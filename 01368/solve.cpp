struct node {
    int x,y,cost;
    node(int x, int y, int cost) {
        this->x = x, this->y = y, this->cost = cost;
    }
    int operator<(const node& other) const {
        return cost > other.cost;
    }
};
class Solution {
public:
    int dx[5] = {0, 1, -1, 0, 0};
    int dy[5] = {0, 0, 0, 1, -1};
    int minCost(vector<vector<int>>& grid) {
        int n = grid.size();
        if (n == 0) return 0;
        int m = grid[0].size();
        priority_queue<node> pq;
        vector<vector<bool>> vis(n, vector<bool>(m, false));
        pq.push(node(0, 0, 0));
        int nx, ny, nc;
        while(!pq.empty()) {
            node now = pq.top();
            pq.pop();
            if (vis[now.y][now.x] == true) continue;
            vis[now.y][now.x] = true;
            if (now.x == m - 1 && now.y == n - 1) return now.cost;
            for (int i = 1; i <= 4; ++i) {
                nx = now.x + dx[i];
                ny = now.y + dy[i];
                nc = now.cost + (grid[now.y][now.x] == i ? 0 : 1);
                if (nx < 0 || ny < 0 || nx >= m || ny >= n || vis[ny][nx] == true) {
                    continue;
                }
                pq.push(node(nx, ny, nc));
            }
        }
        return 0;
    }
};