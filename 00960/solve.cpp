class Solution {
public:
    int minDeletionSize(vector<string>& A) {
        int size = A.size();
        if (size == 0) {
            return 0;
        }
        int ans = 1;
        int len = A[0].length();
        vector<int> dp(len, 1);
        for (int i = 1; i < len; ++i) {
            for (int j = 0; j < i; ++j) {
                bool succ = true;
                for (int k = 0; k < size; ++k) {
                    if (A[k][j] > A[k][i]) {
                        succ = false;
                        break;
                    }
                }
                if (succ) {
                    dp[i] = max(dp[i], dp[j] + 1);
                    ans = max(dp[i], ans);
                }
            }
        }
        return len - ans;
    }
};