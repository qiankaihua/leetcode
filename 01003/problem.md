# Check If Word Is Valid After Substitutions

[题目链接](https://leetcode.com/problems/check-if-word-is-valid-after-substitutions/)

<diff>Medium</diff>

给一串由abc构成的字符串，问是否是一个合法的字符串

该串是合法的当且仅当可以从字符串中不断的提取连续的“abc”，最后没有多余的字符，例如“
aabcbc -> a "abc" bc -> abc -> ""
