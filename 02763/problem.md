# Sum of Imbalance Numbers of All Subarrays

[题目链接](https://leetcode.com/problems/sum-of-imbalance-numbers-of-all-subarrays/)

<diff>Hard</diff>

给一个数组，问它的子数组的 `imbalance number` 之和是多少

`imbalance number` 定义为：
给定一个数组 n，对于任意下标 `0 <= i < n - 1`，如果排序后的数组 `sorted(n)` 满足 `sn[i+1] - sn[i] > 1`则`imbalance number`加一
