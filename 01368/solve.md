可以转换成 bfs 进行搜索：
相邻的格子之间有边，需要切换箭头方向则边权为 1，否则为 0
bfs跑一边就可以了

下面是一个最快的代码，分析后可以知道，他在每一次搜索到一个合法的点之后就用dfs将连通块跑出来放到数组中，其实是将同样cost的点在队列中连起来，而不需要像我代码中一样使用优先队列了。所以队列的插入和移除的复杂度从O(log(N))降低到O(1)了。

```cpp
#define INF 1000005
typedef pair<int, int> pii;
class Solution {
    int n, m, d[105][105], a[105][105];
    int dx[4] = {0, 0, 1, -1};
    int dy[4] = {1, -1, 0, 0};
    queue<pii> q;
public:
    bool valid(int i, int j){
        return i >= 0 && i < n && j >= 0 && j < m;
    }
    void dfs(int i, int j){
        int nx = i + dx[a[i][j]], ny = j + dy[a[i][j]];
        if(valid(nx, ny) && d[nx][ny] == -1){
            d[nx][ny] = d[i][j];
            q.push({nx, ny});
            dfs(nx, ny);
        }
    }
    int minCost(vector<vector<int>>& g) {
        n = g.size();
        m = g[0].size();
        for(int i = 0; i < n; i++) for(int j = 0; j < m; j++) a[i][j] = g[i][j] - 1;
        typedef pair<int, int> pii;
        q = queue<pii>();
        memset(d, -1, sizeof(d));
        
        d[0][0] = 0;
        q.push({0, 0});
        dfs(0, 0);
        
        while(!q.empty()){
            pii cur = q.front();
            q.pop();
            int cx = cur.first, cy = cur.second;
            // cout<<cx<<", "<<cy<<" d = "<<d[cx][cy]<<endl;
            for(int k = 0; k < 4; k++){
                int nx = cx + dx[k], ny = cy + dy[k];
                if(valid(nx, ny) && d[nx][ny] == -1){
                    d[nx][ny] = d[cx][cy] + (a[cx][cy] != k);
                    q.push({nx, ny});
                    dfs(nx, ny);
                }
            }
        }
        return d[n - 1][m - 1];
    }
};
```