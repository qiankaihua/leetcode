class Solution {
public:
    int visiblePoints(vector<vector<int>>& points, int angle, vector<int>& location) {
        const double PI = acos(-1);
        vector<double> angles;
        int pos = 0, x = location[0], y = location[1], ans = 1;
        for (auto p : points) {
            if (p[0] == x && p[1] == y) {
                ++pos;
                continue;
            }
            angles.emplace_back(atan2(p[1] - y, p[0] - x) * 180.0 / PI);
        }
        sort(angles.begin(), angles.end());
        int size = angles.size();
        angles.resize(size * 2);
        for (int i = 0; i < size; ++i) {
            angles[i + size] = angles[i] + 360;
        }
        int l = 0, r = 0;
        size = size * 2;
        while (r < size) {
            if (angles[r] - angles[l] <= angle) {
                ++r;
                continue;
            }
            ans = max(ans, r - l + pos);
            ++l;
        }
        ans = max(ans, r - l + pos);
        return ans;
    }
};