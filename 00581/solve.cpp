class Solution {
public:
    int findUnsortedSubarray(vector<int>& nums) {
        int end, start, maxx = nums[0];
        bool has = false;
        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i] < nums[i - 1]) {
                if (!has) {
                    has = true;
                    start = i - 1;
                }
                while (start > 0 && nums[start - 1] > nums[i]) --start;
            }
            if (has && nums[i] < maxx) {
                end = i;
            }
            maxx = max(maxx, nums[i]);
        }
        if (has) return end - start + 1;
        return 0;
    }
};