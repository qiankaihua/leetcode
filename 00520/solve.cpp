class Solution {
public:
    bool detectCapitalUse(string word) {
        bool hasCap = false, b[2];
        if (word.length() == 0 || word.length() == 1) return true;
        if (word[0] >= 'A' && word[0] <= 'Z') hasCap = true;
        b[0] = b[1] = false;
        for (int i = 1; i < word.length(); ++i) {
            if (!hasCap) {
                if (word[i] >= 'A' && word[i] <= 'Z') return false;
            } else {
                if (word[i] >= 'A' && word[i] <= 'Z') b[0] = true;
                if (word[i] >= 'a' && word[i] <= 'z') b[1] = true;
            }
        }
        return (!hasCap) || (b[0] ^ b[1]);
    }
};