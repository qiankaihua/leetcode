int cmp(int a, int b) {
    if (abs(a) == abs(b)) return a < b;
    return abs(a) < abs(b);
}
class Solution {
public:
    vector<int> findClosestElements(vector<int>& arr, int k, int x) {
        for (int i = arr.size() - 1; i >= 0; --i) arr[i] -= x;
        sort(arr.begin(), arr.end(), cmp);
        arr.resize(k);
        for (int i = arr.size() - 1; i >= 0; --i) arr[i] += x;
        sort(arr.begin(), arr.end());
        return arr;
    }
};