# Number of Burgers with No Waste of Ingredients

[题目链接](https://leetcode.com/problems/number-of-burgers-with-no-waste-of-ingredients/)

<diff>Medium</diff>

给番茄片的数量和芝士片的数量，求出两种汉堡的个数，使得两个材料都不剩下

两个汉堡用材如下：

- Jumbo Burger： 四个番茄片和一个芝士片
- Small Burger： 两个番茄片和一个芝士片
