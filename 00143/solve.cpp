/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
ListNode* reserve(ListNode *head) {
    ListNode *nxt, *now = NULL;
    while (head) {
        nxt = head->next;
        head->next = now;
        now = head;
        head = nxt;
    }
    return now;
}
class Solution {
public:
    void reorderList(ListNode* head) {
        ListNode *mid = head, *fast = head, *tmp, *last = head, *now;
        bool before = true;
        if (!head) return;
        while (fast && fast->next) {
            last = mid;
            mid = mid->next;
            fast = fast->next->next;
        }
        if (fast) {
            last = mid;
            mid = mid->next;
        }
        tmp = new ListNode();
        now = tmp;
        mid = reserve(mid);
        fast = head;
        if (last) last = last->next;
        while (fast != last || mid) {
            if (before) {
                now->next = fast;
                now = now->next;
                fast = fast->next;
            } else {
                now->next = mid;
                now = now->next;
                mid = mid->next;
            }
            before = !before;
        }
        if (now) now->next = NULL;
        head = tmp->next;
    }
};