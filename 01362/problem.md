# Closest Divisors

[题目链接](https://leetcode.com/problems/closest-divisors/)

<diff>Medium</diff>

给一个数字，求出两个数的乘积等于这个数+1或者+2的，返回最接近的两个数