# Sort Integers by The Power Value

[题目链接](https://leetcode.com/problems/sort-integers-by-the-power-value/)

<diff>Medium</diff>

一个数字x的power可以定义为到1需要进行的操作次数，操作可以定义为：

- 如果是偶数，除以二
- 如果是奇数，乘三加一

给一个最小值，最大值和k，求最小值最大值之间power第k大的数，如果power相同则原数小的在前。

case:
Input: lo = 12, hi = 15, k = 2
Output: 13
Explanation: The power of 12 is 9 (12 --> 6 --> 3 --> 10 --> 5 --> 16 --> 8 --> 4 --> 2 --> 1)
The power of 13 is 9
The power of 14 is 17
The power of 15 is 17
