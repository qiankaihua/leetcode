# Strange Printer II

[题目链接](https://leetcode.com/problems/strange-printer-ii/)

<diff>Hard</diff>

给你一个数字标注的矩阵，不同数字代表不同颜色
矩阵最初是没有颜色的，你每次可以用一种颜色画一个小矩阵，用过的颜色就不能再用，问是否能画出标注出来的矩阵
