class Solution {
public:
    int ta, tb, tc, gcdAB, gcdBC, gcdAC, gcdABC;
    int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }
    int nthUglyNumber(int n, int a, int b, int c) {
        int left = 0, right = INT_MAX, mid, flag;
        ta = a;
        tb = b;
        tc = c;
        gcdAB = gcd(a, b);
        gcdBC = gcd(c, b);
        gcdAC = gcd(a, c);
        gcdABC = gcd(a / gcdAB * b, c);
        while (left < right) {
            mid = left + (right - left) / 2;
            flag = checkAns(n, mid);
            if (flag == 0) return mid;
            if (flag < 0)  left = mid + 1;
            else right = mid - 1;
        }
        return left;
    }
    int checkAns(int n, long long ans) {
        long long tmp = ans / ta / (tb / gcdAB) / (tc / gcdABC) - ans / ta / (tc / gcdAC) - ans / ta / (tb / gcdAB) - ans / tc / (tb / gcdBC) + ans / ta + ans / tb + ans / tc;
        if (tmp == n && ans % ta != 0 && ans % tb != 0 && ans % tc != 0) return 1;
        return tmp - n;
    }
};