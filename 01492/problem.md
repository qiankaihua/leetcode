# The kth Factor of n

[题目链接](https://leetcode.com/problems/the-kth-factor-of-n/)

<diff>Medium</diff>

给两个正整数n，k，输出n的第k个因数。如果没有那么多，返回-1.

1 <= k <= n <= 1000
