# Find Array Given Subset Sums

[题目链接](https://leetcode.com/problems/find-array-given-subset-sums/)

<diff>Hard</diff>

给一个数组的全部子数组的和，还原原数组

example：

Input：
`n = 3`
`sums = [-3 -2 -1 0 0 1 2 3]`

Output:
`[1 2 -3]`

`[] [1] [2] [-3] [1 2] [1 -3] [2 -3] [1 2 -3]`
