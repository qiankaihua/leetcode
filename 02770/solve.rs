impl Solution {
    pub fn maximum_jumps(nums: Vec<i32>, target: i32) -> i32 {
        let size = nums.len();
        let mut vec: Vec<i32> = vec![-1; size];
        vec[0] = 0;
        for i in 0..size {
            if vec[i] == -1 {
                continue;
            }
            for j in i + 1..size {
                if Self::abs(nums[i] - nums[j]) <= target {
                    vec[j] = std::cmp::max(vec[j], vec[i] + 1);
                }
            }
        }
        return vec[size - 1];
    }
    fn abs(num: i32) -> i32 {
        if num < 0 {
            return -num;
        }
        num
    }
}
