class Solution {
public:
    int sumImbalanceNumbers(vector<int>& nums) {
        int ans = 0;
        for (int start = 0; start < nums.size(); ++start) {
            int subans = 0;
            int flag[1002] = {0};
            flag[nums[start]] = 1;
            for (int end = start + 1; end < nums.size(); ++end) {
                if (flag[nums[end]] == 1) {
                    ans += subans;
                    continue;
                }
                flag[nums[end]] = 1;
                if (flag[nums[end] - 1] == 1 && flag[nums[end] + 1] == 1) {
                    --subans;
                    ans += subans;
                    continue;
                }
                if (flag[nums[end] - 1] == 1 || flag[nums[end] + 1] == 1) {
                    ans += subans;
                    continue;
                }
                ++subans;
                ans += subans;
            }
        }
        return ans;
    }
};