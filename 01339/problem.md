# Maximum Product of Splitted Binary Tree

[题目链接](https://leetcode.com/problems/maximum-product-of-splitted-binary-tree/)

<diff>Medium</diff>

给一棵树，让你分成两个子树，使得子树和的乘积最大，答案对1e9+7取模。
