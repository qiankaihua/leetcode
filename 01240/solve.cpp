class Solution {
public:
    vector<int> height;
    int sum = 0x7fffffff;
    int size;
    int tilingRectangle(int n, int m) {
        for (int i = 0; i < m; ++i) height.push_back(n);
        size = m;
        dfs(0, 0);
        return sum;
    }
    void dfs(int cnt, int pos) {
        if (cnt >= sum) return;
        if (check()) {
            sum = min(sum, cnt);
            return;
        }
        int maxLen = height[pos];
        int len = 1;
        for (int i = pos + 1; i < size; ++i) {
            if (height[pos] == height[i]) len++;
            else break;
        }
        maxLen = min(maxLen, len);
        for (int i = maxLen; i >= 0; --i) {
            modifyHeight(pos, pos + i, i);
            dfs(cnt + 1, findNextPos());
            modifyHeight(pos, pos + i, -1 * i);
        }
    }
    int findNextPos() {
        int maxH = 0, maxP = -1;
        for (int i = 0; i < size; ++i) {
            if (height[i] > maxH) {
                maxP = i;
                maxH = height[i];
            }
        }
        return maxP;
    }
    void modifyHeight(int from, int to, int add) {
        for (int i = from; i < to; ++i) height[i] -= add;
    }
    bool check() {
        for (int i = 0; i < size; ++i) {
            if (height[i] != 0) return false;
        }
        return true;
    }
};