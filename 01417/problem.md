# Reformat The String

[题目链接](https://leetcode.com/problems/reformat-the-string/)

<diff>Easy</diff>

给一个只用数字和小写字母的字符串，你需要改变他的排序，使得数字和字母互不相连，返回任意满足条件的字符串即可，如果都不能满足条件， 则返回空字符串
