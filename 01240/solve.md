数据范围不大，可以使用DFS搜索+剪枝

height数组存的是每个位置剩下了多少高度需要填充，然后开始遍历

每次找的遍历点是

比较常规的剪枝，例如在当前计数超过目前的全局最小的时候，直接不处理

这题加上这个剪枝就可以过了

还有一个非常重要的剪枝，就是优先填充大的方块，就是25行那种写法

```cpp
        for (int i = maxLen; i >= 0; --i) {
```

| type | Runtime | Memory |
| --- | --- | --- |
| 1 -> max | 532ms | 6.1MB |
| max -> 1 | 4ms | 6.2 MB |

可以看到，仅仅是改变了一个循环方向，整个程序耗时就降到了1%不到，所以写程序还是要很注重细节的处理。


-----

ps： 这题还有一个非常trick的解法

用 dp 来解

```cpp
vector<vector<int>> dp(n + 1, vector<int>(m + 1));    
for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= m; ++j) {
    dp[i][j] = INT_MAX;
    if (i == j) {
        dp[i][j] = 1;
        continue;
    }
    for (int r = 1; r <= i / 2; ++r)
        dp[i][j] = min(dp[i][j], dp[r][j] + dp[i - r][j]);
    for (int c = 1; c <= j / 2; ++c)
        dp[i][j] = min(dp[i][j], dp[i][c] + dp[i][j - c]);
}
```

但是这样是过不去的，因为有一个例外，就是在样例中的 n = 11, m = 13 的时候，答案是6，只需要对这一个值进行特判就好，但是这种做法并不科学，看看就好。