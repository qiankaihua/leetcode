# Minimum Number of Taps to Open to Water a Garden

[题目链接](https://leetcode.com/problems/minimum-number-of-taps-to-open-to-water-a-garden/)

<diff>Hard</diff>

有一个一维的花园，`[0-n]`共有`n+1`个龙头，每个龙头喷水的范围为`range[i]`，求最少需要多少个龙头才能给花园全部喷水，如果全开了也不行，则返回-1。
