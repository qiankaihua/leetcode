# Partition Equal Subset Sum

[题目链接](https://leetcode.com/problems/partition-equal-subset-sum)

<diff>Medium</diff>

给一个数组, 问这个数组是否可以被分割为两个数组, 使得他们的和相等.
