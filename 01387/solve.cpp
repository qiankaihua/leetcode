struct Node {
    int v, pos;
};
int operator<(const Node a, const Node b) {
    if (a.v == b.v) return a.pos < b.pos;
    return a.v < b.v;
}
class Solution {
public:
    unordered_map<int, int> mem;
    int calc(int x) {
        if (mem.find(x) != mem.end()) return mem[x];
        if (x == 1) mem[x] = 0;
        else if (x % 2 == 0) mem[x] = 1 + calc(x / 2);
        else mem[x] = 1 + calc(x * 3 + 1);
        return mem[x];
    }
    int getKth(int lo, int hi, int k) {
        vector<Node> ans(hi - lo + 1);
        for (int i = lo; i <= hi; ++i) ans[i - lo] = Node{calc(i), i};
        sort(ans.begin(), ans.end());
        return ans[k - 1].pos;
    }
};