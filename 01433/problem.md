# Check If a String Can Break Another String

[题目链接](https://leetcode.com/problems/check-if-a-string-can-break-another-string/)

<diff>Medium</diff>

给两个等长的字符串，问其中一个能不能`break`另外一个

break的定义为：
两个字符串任意乱序字符，其中有一种排列使得`s1[i] >= s2[i]`恒成立。
