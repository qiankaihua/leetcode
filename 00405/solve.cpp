class Solution {
public:
    string toHex(int num) {
        string ans = "";
        int t;
        unsigned int tmp = num;
        if (tmp == 0) return "0";
        while (tmp) {
            t = tmp & 0xF;
            if (t < 10) {
                ans = "0" + ans;
                ans[0] += t;
            } else {
                ans = "a" + ans;
                ans[0] += t - 10;
            }
            tmp >>= 4;
        }
        return ans;
    }
};