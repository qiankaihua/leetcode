# Contains Duplicate III

[题目链接](https://leetcode.com/problems/contains-duplicate-iii/)

<diff>Medium</diff>

给一个数组，一个k，一个t，问有没有i，j，使得`|i-j|`不超过k，并且`|num[i]-num[j]|`不超过t