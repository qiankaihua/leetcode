class Solution {
public:
    bool buddyStrings(string s, string goal) {
        char first, second;
        int cnt = 0, charNum[26] = {0};
        bool hasDup = false;
        if (s.length() != goal.length()) return false;
        for (int i = 0; i < s.length(); ++i) {
            if (++charNum[s[i] - 'a'] >= 2) hasDup = true;
            if (s[i] != goal[i]) {
                ++cnt;
                if (cnt > 2) return false;
                if (cnt == 1) first = s[i], second = goal[i];
                else if (s[i] != second || goal[i] != first) return false;
            }
        }
        return cnt == 2 || (hasDup && cnt == 0);
    }
};