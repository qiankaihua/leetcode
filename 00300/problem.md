#  Longest Increasing Subsequence

[题目链接](https://leetcode.com/problems/longest-increasing-subsequence/)

<diff>Medium</diff>

给一个数字的数字，求最长上升子序列的长度

要求O(N^2)的时间复杂度

最好在O(Nlog(N))
