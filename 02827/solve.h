#ifdef LEETCODE_INCLUDE_TEST
#include "../0test/include.h"
#endif


class Solution {
public:
    int numberOfBeautifulIntegers(int low, int high, int k) {
        k_ = k;
        memset(dp_, -1, sizeof(dp_));
        int high_count = dp(0, 0, 0, 0, true, true, to_string(high));
        memset(dp_, -1, sizeof(dp_));
        int low_count = dp(0, 0, 0, 0, true, true, to_string(low - 1));
        return high_count - low_count;
    }
private:
    int dp(int now_index, int remain, int odd_num, int even_num, bool is_now_max, bool is_zero, const string& raw_string) {
        if (now_index == raw_string.size()) {
            return (remain == 0 && odd_num == even_num) ? 1 : 0;
        }
        if (dp_[now_index][odd_num][even_num][remain][is_now_max] != -1) {
            return dp_[now_index][odd_num][even_num][remain][is_now_max];
        }
        int nxt_max = is_now_max ? raw_string[now_index] - '0' : 9;
        int count = 0;
        int nxt_remain = (remain * 10) % k_;
        int nxt_even_num = even_num;
        int nxt_odd_num = odd_num;
        for (int i = 0; i <= nxt_max; ++i) {
            if (!is_zero || i != 0) {
                if ((i & 1) == 1) {
                    nxt_even_num = even_num + 1;
                    nxt_odd_num = odd_num;
                } else {
                    nxt_even_num = even_num;
                    nxt_odd_num = odd_num + 1;
                }
            }
            count += dp(now_index + 1, (nxt_remain + i) % k_, nxt_odd_num, nxt_even_num, is_now_max && i == nxt_max, is_zero && i == 0, raw_string);
        }
        dp_[now_index][odd_num][even_num][remain][is_now_max] = count;
        return count;
    }
private:
    static const int max_digit = 10;
    static const int max_k = 20;
    int k_ = 0;
    int dp_[Solution::max_digit][Solution::max_digit][Solution::max_digit][Solution::max_k + 1][2];
};