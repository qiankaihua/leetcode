# Trapping Rain Water

[题目链接](https://leetcode.com/problems/trapping-rain-water/)

<diff>Hard</diff>

给一个数组, 每一位表示当前格子的高度, 求这个高度地形能留存住多少格水.
