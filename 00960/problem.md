# Delete Columns to Make Sorted III

[题目链接](https://leetcode.com/problems/delete-columns-to-make-sorted-iii/)

<diff>Hard</diff>

给一个字符串数组，长度都相同，问你最少删掉多少列才能使得每个字符串中的字母都是非降序的。
