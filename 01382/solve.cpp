/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* balanceBST(TreeNode* root) {
        vector<int> num;
        getAll(root, num);
        return buildTree(num, 0, num.size() - 1);
    }
    TreeNode* buildTree(vector<int>& nums, int start, int end) {
        if (start > end) return NULL;
        int mid = start + (end - start) / 2;
        TreeNode* now = new TreeNode(nums[mid], buildTree(nums, start, mid - 1), buildTree(nums, mid + 1, end));
        return now;
    }
    void getAll(TreeNode* root, vector<int>& nums) {
        if (root == NULL) return;
        getAll(root->left, nums);
        nums.push_back(root->val);
        getAll(root->right, nums);
    }
    
};


// 利用原有空间

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* balanceBST(TreeNode* root) {
        vector<TreeNode*> num;
        getAll(root, num);
        return buildTree(num, 0, num.size() - 1);
    }
    TreeNode* buildTree(vector<TreeNode*>& nums, int start, int end) {
        if (start > end) return NULL;
        int mid = start + (end - start) / 2;
        TreeNode* now = nums[mid];
        now->left = buildTree(nums, start, mid - 1);
        now->right = buildTree(nums, mid + 1, end);
        return now;
    }
    void getAll(TreeNode* root, vector<TreeNode*>& nums) {
        if (root == NULL) return;
        getAll(root->left, nums);
        nums.push_back(root);
        getAll(root->right, nums);
    }
    
};