class Solution {
public:
    int countNegatives(vector<vector<int>>& grid) {
        int cnt = 0, last, size, pos;
        if (grid.size() < 0) return 0;
        size = grid[0].size();
        int len = grid.size();
        last = size - 1;
        for (int i = 0; i < len; ++i) {
            pos = findFirstSmallThenZero(grid[i], 0, last);
            last = min(pos, size - 1);
            cnt += size - pos;
        }
        return cnt;
    }
    int findFirstSmallThenZero(vector<int>& list, int start, int end) {
        int mid;
        while (start <= end) {
            mid = start + (end - start) / 2;
            if (list[mid] < 0) end = mid - 1;
            else start = mid + 1;
        }
        return start;
    }
};