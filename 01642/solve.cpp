class Solution {
public:
    int furthestBuilding(vector<int> &heights, int bricks, int ladders) {
        std::priority_queue<int> pq;
        int res = 0, left = bricks, left_ladders = ladders;
        for (int i = 1; i < heights.size(); i++) {
            if (heights[i] <= heights[i - 1]) {
                ++res;
                continue;
            }
            auto diff = heights[i] - heights[i - 1];
            if (left >= diff) {
                pq.push(diff);
                left -= diff;
                ++res;
                continue;
            }
            if ((pq.empty() || pq.top() <= diff) && left_ladders > 0) {
                --left_ladders;
                ++res;
                continue;
            }
            if (!pq.empty() && left < diff && left_ladders > 0) {
                --left_ladders;
                left += pq.top();
                pq.pop();
            }
            if (left >= diff) {
                pq.push(diff);
                left -= diff;
                ++res;
                continue;
            }
            return res;
        }
        return res;
    }
};