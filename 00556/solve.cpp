int cmp(int a, int b) {
    return a > b;
}
class Solution {
public:
    vector<int> intToVector(int n) {
        vector<int> v;
        while (n) {
            v.push_back(n % 10);
            n /= 10;
        }
        return v;
    }
    int vectorToInt(vector<int> &v) {
        int ans = 0;
        int max_pre = INT_MAX / 10, max_num = INT_MAX % 10;
        for(int i = v.size() - 1; i >= 0; --i) {
            if (ans > max_pre || (ans == max_pre && v[i] > max_num)) return -1;
            ans *= 10;
            ans += v[i];
        }
        return ans;
    }
    int nextGreaterElement(int n) {
        if (n < 10) return -1;
        vector<int> v = intToVector(n);
        int size = v.size();
        for (int i = 0; i < size - 1; ++i) {
            if (v[i] > v[i + 1]) {
                int minNum = v[i], minPos = i;
                for (int j = 0; j <= i; ++j) {
                    if (minNum > v[j] && v[j] > v[i + 1]) minNum = v[j], minPos = j;
                }
                v[minPos] = v[i + 1];
                v[i + 1] = minNum;
                sort(v.begin(), v.begin() + i + 1, cmp);
                return vectorToInt(v);
            }
        }
        return -1;
    }
};