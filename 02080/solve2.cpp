class RangeFreqQuery {
public:
    RangeFreqQuery(vector<int>& arr) {
        for (int idx = 0; idx < arr.size(); ++idx) {
            mp_[arr[idx]].emplace_back(idx);
        }
    }
    
    int query(int left, int right, int value) {
        auto it = mp_.find(value);
        if (it == mp_.end()) {
            return 0;
        }
        auto lb = std::lower_bound(it->second.begin(), it->second.end(), left);
        auto ub = std::upper_bound(lb, it->second.end(), right);
        return ub - lb;
    }

private:
    unordered_map<int, vector<int>> mp_;
};

/**
 * Your RangeFreqQuery object will be instantiated and called as such:
 * RangeFreqQuery* obj = new RangeFreqQuery(arr);
 * int param_1 = obj->query(left,right,value);
 */