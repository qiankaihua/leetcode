# Maximum Number of Jumps to Reach the Last Index

[题目链接](https://leetcode.com/problems/maximum-number-of-jumps-to-reach-the-last-index)

<diff>Medium</diff>

给一个数组, 计算跳转到最后一个位置的最大次数, 跳转有如下两个要求:
1. 只能往后跳
2. 跳跃点和当前点的值差的绝对值不能超过 target