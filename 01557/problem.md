# Minimum Number of Vertices to Reach All Nodes

[题目链接](https://leetcode.com/problems/minimum-number-of-vertices-to-reach-all-nodes/submissions/)

<diff>Medium</diff>

给一个有向无环图，问最少从多少个节点开始能访问图的所有节点。