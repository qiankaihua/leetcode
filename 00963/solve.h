#ifdef LEETCODE_INCLUDE_TEST
#include "../0test/include.h"
#endif

class Solution {
public:
    double minAreaFreeRect(vector<vector<int>>& points) {
        double result = -1;
        for (int i = 0; i < points.size(); ++i) {
            for (int j = i + 1; j < points.size(); ++j) {
                for (int k = j + 1; k < points.size(); ++k) {
                    for (int l = j + 1; l < points.size(); ++l) {
                        bool found = false;
                        double sub_res;
                        sub_res = getArea(points[i], points[j], points[k], points[l], found);
                        if (found) {
                            if (result < 0) result = sub_res;
                            else result = min(result, sub_res);
                            continue;
                        }
                        sub_res = getArea(points[i], points[j], points[l], points[k], found);
                        if (found) {
                            if (result < 0) result = sub_res;
                            else result = min(result, sub_res);
                            continue;
                        }
                        sub_res = getArea(points[i], points[k], points[j], points[l], found);
                        if (found) {
                            if (result < 0) result = sub_res;
                            else result = min(result, sub_res);
                            continue;
                        }
                        sub_res = getArea(points[i], points[k], points[l], points[j], found);
                        if (found) {
                            if (result < 0) result = sub_res;
                            else result = min(result, sub_res);
                            continue;
                        }
                        sub_res = getArea(points[i], points[l], points[j], points[k], found);
                        if (found) {
                            if (result < 0) result = sub_res;
                            else result = min(result, sub_res);
                            continue;
                        }
                        sub_res = getArea(points[i], points[l], points[k], points[j], found);
                        if (found) {
                            if (result < 0) result = sub_res;
                            else result = min(result, sub_res);
                            continue;
                        }
                    }
                }
            }
        }
        if (result < 0) return 0;
        return result;
    }

private:
    double getArea(const vector<int>& p1, const vector<int>& p2, const vector<int>& p3, const vector<int>& p4, bool& ok) {
        ok = false;
        if (p1[0] - p2[0] == p3[0] - p4[0] && p1[1] - p2[1] == p3[1] - p4[1]) {
            int64_t l1 = (p1[0] - p2[0]) * (p1[0] - p2[0]) + (p1[1] - p2[1]) * (p1[1] - p2[1]);
            int64_t l2 = (p1[0] - p3[0]) * (p1[0] - p3[0]) + (p1[1] - p3[1]) * (p1[1] - p3[1]);
            if (l1 + l2 != (p2[0] - p3[0]) * (p2[0] - p3[0]) + (p2[1] - p3[1]) * (p2[1] - p3[1])) {
                return 0.0;
            }
            ok = true;
            return sqrt(l1) * sqrt(l2);
        }
        return 0.0;
    }
};