最开始的想法是记录每一种数字的出现的下标,由于从前往后遍历,所以下标天然有序,可以直接进行二分查找定位区间范围,相减即是答案.
但是初次处理的时候不停地 tle,计算复杂度是可以满足的,但是就是过不去,迫于无奈加了个缓存就过了,这也是 solve1 的做法.
看了别人的代码发现和自己一开始的想法完全一致,但是别人就是很快,枚举了每一个不同发现,重点在于 `map[value]` 和 `map.find(value)` 这两步,前者改为后者之后就能过了,这部分确实是我没有仔细考虑到的点,前者的操作包含了在 value 不存在的时候创建对应 kv,而后者只是单纯的查找,这部分的性能差异还是非常大的.
另外一部分优化是,查找 right 的时候,可以从 left 查找完成的 iter 开始查找,虽然对 tle 的 case 影响不大就是了.

一开始也考虑过线段树 or 树状数组, 但是这里需要每个节点保存当前范围的频率,感觉空间利用率太低了. 题解中确实也有人这么实现, 整体耗时和内存确实都不太理想.