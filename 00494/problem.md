# Target Sum

[题目链接](https://leetcode.com/problems/target-sum/)

<diff>Medium</diff>

给你一个数组和一个目标值，你可以在数组每一个数前加上正号或是负号，问有几种标号方式能获得目标值

数组最多20个，数组元素和不超过1000
