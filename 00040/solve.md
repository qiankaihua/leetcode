简单的dfs，注意剪枝就好

这里有一个细节，就是如果不选取这一个数，就直接跳到下一个不相同的数，因为要求答案没有重复，所有相同的数如果有跳过的话，后面的都可以跳过，如果选取后面的数，则和选择前面的数效果是一样的，所以每一种数都是选择最头上的n个。
