# Find All Numbers Disappeared in an Array

[题目链接](https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/submissions/)

<diff>Easy</diff>

给你一个数字在`[1-n]`的数组，数组大小是`n`，数字可以重复，求没出现过的数字。

不使用额外变量，并且`O(n)`的时间
