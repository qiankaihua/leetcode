# Remove Nth Node From End of List

[题目链接](https://leetcode.com/problems/remove-nth-node-from-end-of-list)

<diff>Medium</diff>

给一个链表, 给一个数字 n, 要求删除链表倒数第 n 个数. 保证 n 不超过链表长度.
