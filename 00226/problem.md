# Invert Binary Tree

[题目链接](https://leetcode.com/problems/invert-binary-tree/)

<diff>Easy</diff>

给定一个树的根节点，翻转整棵树（所有左右儿子节点交换）
