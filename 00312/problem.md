# Burst Balloons

[题目链接](https://leetcode.com/problems/burst-balloons/)

<diff>Hard</diff>

给一串数字，代表每个气球的价值，打破任意一个气球，可以获得它和它相邻两个气球价值的乘积，之后左右两个气球变得相邻，如果打破的是端上的气球，则没气球的一边价值用1代替。

问最大获得多少价值
