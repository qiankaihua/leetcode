// 1600ms
class Solution {
public:
    int kSimilarity(string s1, string s2) {
        return bfs(s1, s2);
    }
private:
    unordered_map<string, int> mp;
    unordered_map<string, int>::iterator iter;
    queue<string> q;
    inline void swap(string &s, int i, int j) {
        auto c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
    int bfs(string &s1, string &s2) {
        if (s1 == s2) return 0;
        string t1 = "", t2 = "";
        for (int i = 0; i < s1.length(); i++) {
            if (s1[i] != s2[i]) {
                t1 += s1[i];
                t2 += s2[i];
            }
        }
        mp.clear();
        mp[s1] = 1;
        q.push(t1);
        int step = 0;
        // 每一次都处理一位，只有这一位相同，才会放入队列，剪枝
        while (!q.empty()) {
            int size = q.size();
            while(size--) {
                auto &t = q.front();
                for (int i = step; i < t.length(); i++) {
                    for (int j = i + 1; j < t.length(); j++) {
                        if (t[i] == t[j]) continue;
                        swap(t, i, j);
                        if (t == t2) return step + 1;
                        if (mp.find(t) == mp.end() &&  t[step] == t2[step]) {
                            q.push(t);
                            mp[t] = step + 1;
                        }
                        swap(t, i, j);
                    }
                }
                q.pop();
            }
            ++step;
        }
        return 0;
    }
};



// 7ms dfs
class Solution {
    int helper(int index, string& s1, string& s2){
        int n = s1.length();
        if(index == n){
            return 0;
        }
        if(s1[index] == s2[index]){
            return helper(index + 1, s1, s2);
        }
        
        int res = INT_MAX;
        for(int k = index + 1; k < n; k++){
            if(s1[k] == s2[index] && s2[k] == s1[index]){
                swap(s1[k], s1[index]);
                res = helper(index + 1, s1, s2) + 1;
                swap(s1[k], s1[index]);
                break;
            }
        }
        if (res != INT_MAX)
            return res;
        
        for(int k = index + 1; k < n; k++){
            if(s1[k] == s2[index]){
                swap(s1[k], s1[index]);
                res = min(res,1 + helper(index + 1, s1, s2));
                swap(s1[k], s1[index]);
            }
        }
        
        return res;
    }
public:
    int kSimilarity(string s1, string s2) {
        return helper(0,s1,s2);
    }
};