# Reorder List

[题目链接](https://leetcode.com/problems/reorder-list/)

<diff>Medium</diff>

给一个链表L：

L: L0→L1→…→Ln-1→Ln

将其转换成下面的形式：

L0→Ln→L1→Ln-1→L2→Ln-2→…

Example：

输入 1->2->3->4, 输入出 1->4->2->3.

输入 1->2->3->4->5, 输出 1->5->2->4->3.