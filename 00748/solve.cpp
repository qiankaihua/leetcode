class Solution {
public:
    int getIndex(char c) {
        if (c >= 'a' && c <= 'z') return c - 'a';
        if (c >= 'A' && c <= 'Z') return c - 'A';
        return 26;
    }
    void cntLicensePlate(vector<int>& cnt, string& licensePlate) {
        for (int i = licensePlate.length() - 1; i >= 0; --i) {
            cnt[getIndex(licensePlate[i])]++;
        }
    }
    bool checkString(vector<int> cnt, string& test) {
        for (int i = test.length() - 1; i >= 0; --i) {
            cnt[getIndex(test[i])]--;
        }
        for (int i = 0; i < 26; ++i) {
            if (cnt[i] > 0) return false;
        }
        return true;
    }
    string shortestCompletingWord(string licensePlate, vector<string>& words) {
        vector<int> cnt(27, 0);
        cntLicensePlate(cnt, licensePlate);
        int minLen = INT_MAX, ansPos = -1;
        for (int i = 0; i < words.size(); ++i) {
            if (words[i].length() >= minLen) continue;
            if (checkString(cnt, words[i])) {
                minLen = words[i].length();
                ansPos = i;
            }
        }
        return words[ansPos];
    }
};