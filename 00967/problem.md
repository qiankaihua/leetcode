# Least Operators to Express Number

[题目链接](https://leetcode.com/problems/numbers-with-same-consecutive-differences/)

<diff>Medium</diff>

给定位数 n 和差值 k, 要求获得所有长度为 n 位, 并且 相邻两位差距恰好为 k 的所有数据.