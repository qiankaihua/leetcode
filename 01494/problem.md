# Parallel Courses II

[题目链接](https://leetcode.com/problems/parallel-courses-ii/)

<diff>Hard</diff>

给一个有向无环图，代表课程依赖关系，每学期可以学习k门课程，问最少几学期学完