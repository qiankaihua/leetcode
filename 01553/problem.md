# Minimum Number of Days to Eat N Oranges

[题目链接](https://leetcode.com/problems/minimum-number-of-days-to-eat-n-oranges/)

<diff>Hard</diff>

给一个整数 n，可以有三种操作
1. 减 1
2. 如果 n % 2 == 0，可以直接 / 2
3. 如果 n % 3 == 0, 可以直接 / 3
