struct CacheNode {
    CacheNode *nxt, *pre;
    int val, key;
};
class LRUCache {
private:
    // unordered_map<int, *CacheNode>::iterator mp_it;
    unordered_map<int, CacheNode*> mp;
    CacheNode *head, *tail;
    int cap, has;
    void update(CacheNode* now) {
        now->nxt->pre = now->pre;
        now->pre->nxt = now->nxt;
        head->nxt->pre = now;
        now->nxt = head->nxt;
        head->nxt = now;
        now->pre = head;
    }
    void freeNode(CacheNode* now) {
        --has;
        now->pre->nxt = now->nxt;
        now->nxt->pre = now->pre;
        delete now;
    }
    void createToHead(int key, int val) {
        ++has;
        CacheNode *now = new CacheNode;
        now->val = val;
        now->key = key;
        mp[key] = now;
        now->nxt = head->nxt;
        now->pre = head;
        head->nxt->pre = now;
        head->nxt = now;
    }
public:
    LRUCache(int capacity) {
        head = tail = NULL;
        cap = capacity;
        has = 0;
        head = new CacheNode;
        tail = new CacheNode;
        head->nxt = tail;
        tail->pre = head;
    }
    
    int get(int key) {
        auto it = mp.find(key);
        if (it == mp.end()) return -1;
        CacheNode *now = it->second;
        update(now);
        return now->val;
    }
    
    void put(int key, int value) {
        auto it = mp.find(key);
        if (it != mp.end()) {
            auto now = it->second;
            update(now);
            now->val = value;
            return;
        }
        if (has == cap) {
            it = mp.find(tail->pre->key);
            freeNode(it->second);
            mp.erase(it);
        }
        createToHead(key, value);
    }
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */