# Maximum Number of Visible Points

[题目链接](https://leetcode.com/problems/maximum-number-of-visible-points/)

<diff>Hard</diff>

给一个vector代表点的坐标，然后给一个角度，再给一个起始坐标点，问在这个起始坐标点，这个角度范围，最多能看到多少点。
点和起点重合的时候永远能看到。