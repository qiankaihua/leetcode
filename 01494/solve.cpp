class Solution {
public:
    /**
     * 参数含义：
     * pos: 课程位置，从0开始
     * pick: 已经选择课程数
     * total: 总课程数
     * totalPick: 最多选择课程数
     * startMask: 已学习课程mask
     * nowMask: 当前选择课程mask
     * needMask: 可以学习课程mask
     * dp: dp数组
     */
    void sovleOne(int pos, int pick, int total, int totalPick, int startMask, int nowMask, int needMask, vector<int> &dp) {
        // 如果遍历完了或者选择课程数到达上限了，更新dp数组
        if (pos == total || pick == totalPick) {
            dp[startMask | nowMask] = min(dp[startMask | nowMask], dp[startMask] + 1);
            return;
        }
        // 对于每一个课程，可以选择不学
        sovleOne(pos + 1, pick, total, totalPick, startMask, nowMask, needMask, dp);
        // 对于刚好可以学习的课程，可以选择学习
        if ((needMask >> pos) & 1) {
            sovleOne(pos + 1, pick + 1, total, totalPick, startMask, nowMask | (1 << pos), needMask, dp);
        }
    }
    int minNumberOfSemesters(int n, vector<vector<int>>& dependencies, int k) {
        vector<int>pre (n, 0);
        for (auto dep : dependencies) {
            pre[dep[1] - 1] |= (1 << (dep[0] - 1));
        }
        int types = 1 << n;
        // 初始化，只有一节课都没学习的为0
        vector<int>dp (types, INT_MAX);
        dp[0] = 0;
        for (int i = 0; i < types; ++i) {
            if (dp[i] == INT_MAX) continue;
            int needMask = 0;
            for (int j = 0; j < n; ++j) {
                // 如果没学习过 并且 前提课程都已经学完
                if (!((i >> j) & 1) && ((i & pre[j]) == pre[j])) {
                    needMask |= (1 << j);
                }
            }
            sovleOne(0, 0, n, k, i, 0, needMask, dp);
        }
        return dp[types - 1];
    }
};