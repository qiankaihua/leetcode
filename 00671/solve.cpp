/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
private:
    void check(TreeNode* root, int &ans, int minn) {
        if (root == NULL) return;
        if (root->val != minn) {
            if (ans == -1) ans = root->val;
            else ans = min(ans, root->val);
        }
        check(root->left, ans, minn);
        check(root->right, ans, minn);
    }
public:
    int findSecondMinimumValue(TreeNode* root) {
        int ans = -1;
        if (root == NULL) return ans;
        check(root, ans, root->val);
        return ans;
    }
};