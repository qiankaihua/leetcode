很容易想到记忆化搜索, dfs + memory 一下子就写完了, 但是第一次运行时死循环了, 发现是 x^n - target > target 的时候, 例如 x=3, target = 5, target 会在 4 和 5 之间不停重复.
实在想不到怎么解决,去看了下别人的题解,发现所有题解都有 x^n - target > target 这一个条件,确实加上就过了,但是没有找到一个题解是解释了这个条件为什么是正确的, 自己证明了两个晚上总算是整出来了.
https://leetcode.com/problems/least-operators-to-express-number/solutions/5727541/c-memory-dfs-with-prove-why-sum-target-target-correct/

另外搜证明过程的时候发现了另外一种思路,感觉也非常巧妙,补充在 solve2

https://www.acwing.com/solution/content/756/

1. 我们可以只考虑每次加上或者减去一个数，这个数为 1, x, x^2, x^3, ...。相当于按 x 进制表示 target，但不同点是进制位上允许负数。
2. 不妨从最低位，即需要多少个 1 开始考虑，假设 target 模 x 的余数为 r，此时有两种选择：
    * 加上 r 个 1，此时问题可以转为到次低位，然后考虑 x 的个数。这可以看做 target = target / x，然后重新变到最低位考虑。
    * 减去 x - r 个 1，此时问题同样转到次低位，然后考虑 x 的个数。这可以看做 target = target/x + 1（这里加 1 时因为最低位补全了一个完整的   x）。然后重新变到到最低位考虑。
3. 递归出口为 target 为 1 时，只需要补一个 x^depth；当 target 为 0 时，我们不需要加上或减去任何数字。出口前，需要去除最开头的正好。
4. 这里可以采用一个哈希表，来记录递归过程中出现的重复状态，即 target 在 depth 层时所需要的最少操作符数量。
