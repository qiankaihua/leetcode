#ifdef LEETCODE_INCLUDE_TEST
#include "../0test/include.h"
#endif

class Solution {
public:
    int numberOfEmployeesWhoMetTarget(vector<int>& hours, int target) {
        int res = 0;
        for (int h : hours) {
            if (h >= target) {
                ++res;
            }
        }
        return res;
    }
};