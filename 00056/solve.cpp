class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        vector<vector<int>> ans;
        if (intervals.size() == 0) return ans;
        sort(intervals.begin(), intervals.end());
        int begin = -1, end = 0;
        for (int i = 0; i < intervals.size(); ++i) {
            if (begin == -1) begin = intervals[i][0], end = intervals[i][1];
            else if (intervals[i][0] <= end) end = max(end, intervals[i][1]);
            else {
                ans.push_back({begin, end});
                begin = intervals[i][0];
                end = intervals[i][1];
            }
        }
        ans.push_back({begin, end});
        return ans;
    }
};