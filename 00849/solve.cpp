class Solution {
public:
    int maxDistToClosest(vector<int>& seats) {
        int begin = 0, maxx = 0, now = 0;
        bool isBegin = true;
        for (int i = 0; i < seats.size(); ++i) {
            if (seats[i] == 0) {
                ++now;
            } else {
                if (isBegin) begin = now, isBegin = false;
                maxx = max(maxx, now);
                now = 0;
            }
        }
        return max((maxx + 1) / 2, max(begin, now));
    }
};