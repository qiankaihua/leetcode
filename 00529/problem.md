# Minesweeper

[题目链接](https://leetcode.com/problems/minesweeper/submissions/)

<diff>Medium</diff>

给一个二维数组用来表示扫雷的地图，E代表没点过的空节点，M代表地雷，B代表点开的空节点，X代表点开的地雷

给一个点击点，返回点击后的地图

1. 点击M，M变成X
2. 点击E，如果周围有地雷，变成地雷数量(1~8)
3. 点击E，如果周围没有地雷，变成B，并且递归处理