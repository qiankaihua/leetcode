class Solution {
public:
    vector<int> recoverArray(int n, vector<int>& sums) {
        if (sums.size() == 0) {
            return {};
        }
        sort(sums.begin(), sums.end());
        int minn = -sums[0];
        for (int i = 0; i < sums.size(); ++i) {
            sums[i] += minn;
        }
        auto v = doPositive(sums);
        dfs(v, minn, 0, v.size(), 0);
        return v;
    }
    vector<int> doPositive(vector<int>& sums) {
        vector<int> ans;
        vector<int> adder;
        map<int, int> mp;
        for (int i = 1, v = 0; i < sums.size(); i++) {
            v = sums[i];
            if (mp.find(v) != mp.end()) {
                if (mp[v] > 0) {
                    mp[v]--;
                    continue;
                }
            }
            for (int j = adder.size() - 1, a = 0; j >= 0; --j) {
                a = adder[j];
                adder.emplace_back(a + v);
                if (mp.find(v + a) == mp.end()) {
                    mp[v + a] = 1;
                } else {
                    mp[v + a]++;
                }
            }
            ans.emplace_back(v);
            adder.emplace_back(v);
        }
        return ans;
    }
    bool dfs(vector<int> &values, int target, int pos, int n, int now) {
        if (now == target) {
            return true;
        }
        if (pos == n) {
            return false;
        }
        if (dfs(values, target, pos + 1, n, now)) {
            return true;
        }
        values[pos] *= -1;
        if (dfs(values, target, pos + 1, n, now - values[pos])) {
            return true;
        }
        values[pos] *= -1;
        return false;
    }
};