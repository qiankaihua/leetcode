# Swap Adjacent in LR String

[题目链接](https://leetcode.com/problems/swap-adjacent-in-lr-string/)

<diff>Medium</diff>

给两个包含L,R,X的字符串start，end，问是否能从start转换为end。字符L只能向左移，R只能向右移。
