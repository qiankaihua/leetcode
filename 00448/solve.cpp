class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        vector<int> ans;
        for (int i = nums.size() - 1; i >= 0; --i) {
            if (nums[abs(nums[i]) - 1] > 0)
                nums[abs(nums[i]) - 1] = -nums[abs(nums[i]) - 1];
        }
        
        for (int i = nums.size() - 1; i >= 0; --i) {
            if (nums[i] > 0)
                ans.push_back(i + 1);
        }
        
        return ans;
    }
};

// 更新一种使用swap的做法
class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        vector<int> ans;
        for (int i = 0; i < nums.size(); ++i) {
            while (nums[nums[i] - 1] != nums[i]) {
                swap(nums[i], nums[nums[i] - 1]);
            }
        }
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] != i + 1) {
                ans.emplace_back(i + 1);
            }
        }
        return std::move(ans);
    }
};