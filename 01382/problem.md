# Balance a Binary Search Tree

[题目链接](https://leetcode.com/problems/balance-a-binary-search-tree/)

<diff>Medium</diff>

给一颗二叉搜索树，重构成为一颗平衡二叉搜索树
