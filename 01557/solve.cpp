class Solution {
public:
    vector<int> findSmallestSetOfVertices(int n, vector<vector<int>>& edges) {
        vector<bool> c(n, false);
        for (auto e : edges) {
            c[e[1]] = true;
        }
        vector<int> ans;
        for (int i = 0; i < n; ++i) {
            if (!c[i]) ans.emplace_back(i);
        }
        return ans;
    }
};