impl Solution {
    pub fn max_sub_array(nums: Vec<i32>) -> i32 {
        let mut ans = 0;
        let mut before: i32 = 0;
        let mut now = 0;
        let mut max = nums[0];
        for i in nums.iter() {
            before = now;
            if *i <= 0 {
                now = 0.max(before + i);
            } else {
                now = (*i).max(before + i)
            }
            ans = ans.max(now);
            max = max.max(*i);
        }
        if max < 0 {
            return max;
        } else {
            return ans;
        }
    }
}
