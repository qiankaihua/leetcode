class Solution {
public:
    int matrixScore(vector<vector<int>>& A) {
        int n, m;
        n = A.size();
        if (n == 0) return 0;
        m = A[0].size();
        if (m == 0) return 0;
        vector<int> cnt(m, 0);
        cnt[0] = n;
        for (int i = 0; i < n; ++i) {
            if (A[i][0] == 1) {
                for (int j = 1; j < m; ++j) {
                    cnt[j] += A[i][j];
                }
            } else {
                for (int j = 1; j < m; ++j) {
                    cnt[j] += 1 - A[i][j];
                }
            }
        }
        for (int i = 1; i < m; ++i) {
            if (n - cnt[i] > cnt[i]) {
                cnt[i] = n - cnt[i];
            }
        }
        int sum = 0;
        for (int i = 0; i < m; ++i) {
            sum += cnt[i] * (1 << (m - i - 1));
        }
        return sum;
    }
};