class Solution {
public:
    int64_t leastOpsExpressTarget(int64_t x, int64_t target) {
        if (x == 1) return target;
        memory.clear();
        return dfs(x, target);
    }
private:
    unordered_map<int64_t, int64_t> memory;
    int64_t dfs(int64_t x, int64_t target) {
        auto it = memory.find(x);
        if (it != memory.end()) {
            return it->second;
        }
        if (target == 1) {
            return 1;
        }
        if (target == x) {
            return 0;
        }
        int64_t count = 0, tmp = x;
        while (tmp < target) {
            tmp *= x;
            ++count;
        }
        // x * x * ... * x - (...) = target
        int64_t ans1 = INT64_MAX;
        if (target == tmp) {
            ans1 = count;
        } else if (tmp - target < target) {
            ans1 = count + 1 + dfs(x, tmp - target);
        }
        // x * x * ... * x + (...) = target
        int64_t ans2 = INT64_MAX;
        if (target < x) {
            ans2 = 2 + dfs(x, target - 1);
        } else {
            tmp /= x;
            if (target == tmp) {
                ans2 = count - 1;
            } else {
                ans2 = count + dfs(x, target - tmp);
            }
        }
        if (ans1 > ans2) ans1 = ans2;
        memory[target] = ans1;
        return ans1;
    }
};