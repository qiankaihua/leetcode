# Random Pick with Weight

[题目链接](https://leetcode.com/problems/random-pick-with-weight/)

<diff>Medium</diff>

给一个数组，数组的值代表这个下标对应的权重，实现一个函数按照权重随机返回一个下标。
