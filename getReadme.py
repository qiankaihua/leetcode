import os
import re
from pathlib import Path

# 获取题目链接，难度及其名字
def getProLinkAndDiffAndName(filename):
    proLink = ""
    diff = ""
    regexPro = re.compile(r"https://leetcode.com/problems/[\w-]+")
    regexDiff = re.compile(r"<diff>[\w]+</diff>")
    file = open(filename, 'r', encoding='utf-8')
    fc = file.readlines()
    name = ""
    hasName = False
    for line in fc:
        if not hasName:
            if len(line) >= 3 and line[0] == '#' and line[1] == ' ':
                name = line[2:-1]
                hasName = True
        if proLink == "":
            match = regexPro.search(line)
            if match is not None:
                proLink = u'[\[problem link\]]({})'.format(match[0])
        if diff == "":
            match = regexDiff.search(line)
            if match is not None:
                diff = match[0]

    return proLink, diff, name

# 获得一个题目的内容及其难度
def getOneList(dirPath, num, typeName):
    [pro, diff, problemName] = getProLinkAndDiffAndName(dirPath + "problem.md")
    return u'| Problem {} | {} | [\[description\]]({}) | [\[solve\]]({}) | [\[code\]]({}) | {} | {} |'.format(num, problemName, dirPath + "problem.md", dirPath + "solve.md", dirPath + "solve." + typeName, pro, diff), diff

# 生成 README 内容
def getReadme(dirname='./'):
    excludeName = ["00000"]
    solveTypeList = ["cpp", "sql", "sh"]
    total = 0
    easy = 0
    medium = 0
    hard = 0
    unknown = 0
    content = u"# Index \n\n| Problem Id | Problem Name | Description | Solve | Code | Problem Link | Difficult |\n| --- | --- | --- | --- | --- | --- | --- |\n"
    problem = u""
    # 按序遍历文件夹
    path_list = os.listdir(dirname)
    path_list.sort()
    for p in path_list:
        if p in excludeName:
            continue
        if os.path.isdir(p):
            if os.path.exists(dirname + p + '/solve.md'):
                solveType = "cpp"
                for types in solveTypeList:
                    if os.path.exists(dirname + p + '/solve.' + types):
                        solveType = types
                        break
                oneList, diff = getOneList(dirname + p + '/', p, solveType)
                problem = problem + oneList + "\n"
                total = total + 1
                # 判断难度
                if diff.find("Easy") != -1:
                    easy = easy + 1
                elif diff.find("Medium") != -1:
                    medium = medium + 1
                elif diff.find("Hard") != -1:
                    hard = hard + 1
                else:
                    print("unknown diff: " + p)
    content = "solved problem number: `" + str(total) + "`\n\neasy: `" + str(easy) + "` medium: `" + str(medium) + "` hard: `" + str(hard) + "`\n\n" + content + problem
    print('\033[4mtotal: %d\033[0m, \033[4;32measy: %d\033[0m, \033[4;33mmedium: %d\033[0m, \033[4;31mhard %d\033[0m' % (easy + medium + hard, easy, medium, hard))
    return content

if __name__ == '__main__':
    content = getReadme()
    with open('./README.md', 'w') as f:
        f.write(content)
