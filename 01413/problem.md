# Minimum Value to Get Positive Step by Step Sum

[题目链接](https://leetcode.com/problems/minimum-value-to-get-positive-step-by-step-sum/)

<diff>Easy</diff>

给一个数组，从头加到尾，问最少加上几使得中间过程全部大于0，答案至少为1。
