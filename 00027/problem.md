# Remove Element

[题目链接](https://leetcode.com/problems/remove-element/)

<diff>Easy</diff>

给一个vector，和一个int，移除vector里面和int值相同的元素，返回对应的vector长度。