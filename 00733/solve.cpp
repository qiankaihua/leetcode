class Solution {
public:
    int row, col, newColor, color;
    bool canMove(int newX, int newY) {
        if (newX < 0 || newY < 0 || newX >= row || newY >= col) return false;
        return true;
    }
    void dfs(vector<vector<int>>& image, int sr, int sc) {
        if (!canMove(sr, sc) || image[sr][sc] == newColor || image[sr][sc] != color) return;
        image[sr][sc] = newColor;
        dfs(image, sr + 1, sc);
        dfs(image, sr, sc + 1);
        dfs(image, sr - 1, sc);
        dfs(image, sr, sc - 1);
    }
    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int newColor) {
        row = image.size();
        if (row < 0) return image;
        col = image[0].size();
        if (image[sr][sc] == newColor) return image;
        this->newColor = newColor;
        color = image[sr][sc];
        dfs(image, sr, sc);
        return image;
    }
};