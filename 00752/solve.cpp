class Solution {
public:
    inline int toInt(char c) { return c - '0'; }
    inline int toInt(const string& input) { return toInt(input[0]) * 1000 + toInt(input[1]) * 100 + toInt(input[2]) * 10 + toInt(input[3]); }
    int openLock(vector<string>& deadends, string target) {
        vector<int> vis (10000, -1);
        queue<int> q;
        int tg = toInt(target);
        q.push(0);
        for (auto s : deadends) {
            vis[toInt(s)] = -2;
        }
        if (vis[tg] == -2 || vis[0] == -2) return -1;
        if (tg == 0) return 0;
        vis[0] = 0;
        int now, nxt, jud, cg[8] = {1, -1, 10, -10, 100, -100, 1000, -1000}, over[8] = {-10, 10, -100, 100, -1000, 1000, -10000, 10000}, div[8] = {1, 1, 10, 10, 100, 100, 1000, 1000}, judge[8] = {9, 0, 9, 0, 9, 0, 9, 0};
        while (!q.empty()) {
            now = q.front();
            q.pop();
            for (int i = 0; i < 8; ++i) {
                jud = (now / div[i]) % 10;
                nxt = now + cg[i];
                if (jud == judge[i]) {
                    nxt += over[i];
                }
                if (nxt == tg) {
                    return vis[now] + 1;
                }
                if (vis[nxt] != -1) continue;
                vis[nxt] = vis[now] + 1;
                q.push(nxt);
            }
        }
        return -1;
    }
};