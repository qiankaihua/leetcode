class Solution {
public:
    string removeDuplicates(string S) {
        deque<char>c;
        char tmp;
        for (int i = 0; i < S.length(); ++i) {
            if (!c.empty()) {
                tmp = c.back();
                if (tmp == S[i]) {
                    c.pop_back();
                    continue;
                }
            }
            c.push_back(S[i]);
        }
        int size = c.size();
        for (int i = 0; i < size; ++i) {
            S[i] = c.front();
            c.pop_front();
        }
        return S.substr(0, size);
    }
};