class RangeFreqQuery {
public:
    RangeFreqQuery(vector<int>& arr) {
        for (int idx = 0; idx < arr.size(); ++idx) {
            mp_[arr[idx]].emplace_back(idx);
        }
    }
    
    int query(int left, int right, int value) {
        int64_t cache_key = (int64_t(value) << 32) | (int64_t(left) << 16) | int64_t(right);
        auto it = cache_.find(cache_key);
        if (it != cache_.end()) {
            return it->second;
        }
        auto indexArray = mp_[value];
        auto lb = std::lower_bound(indexArray.begin(), indexArray.end(), left);
        auto ub = std::upper_bound(indexArray.begin(), indexArray.end(), right);
        cache_[cache_key] = ub - lb;
        return ub - lb;
    }

private:
    unordered_map<int, vector<int>> mp_;
    unordered_map<int64_t, int> cache_;
};

/**
 * Your RangeFreqQuery object will be instantiated and called as such:
 * RangeFreqQuery* obj = new RangeFreqQuery(arr);
 * int param_1 = obj->query(left,right,value);
 */