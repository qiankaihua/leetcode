# Masking Personal Information

[题目链接](https://leetcode.com/problems/masking-personal-information/)

<diff>Medium</diff>

给一个字符串，可能是数字，也可能是电话号码，电话号码除了后四位加密，邮件前半部分只留首尾两个字母（具体逻辑去网页看）。
