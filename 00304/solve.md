类的构造函数中预处理出从(0, 0) 到 (i, j)的元素和，保存在 (i + 1, j +1) 的 sum 数组中，然后求和的时候返回：

`sum[row2 + 1][col2 + 1] - sum[row1][col2 + 1] - sum[row2 + 1][col1] + sum[row1][col1]`