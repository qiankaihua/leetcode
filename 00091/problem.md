# Decode Ways

[题目链接](https://leetcode.com/problems/decode-ways/)

<diff>Medium</diff>

给一个0-9构成的字符串，1-26对应a-z，问这个字符串代表的数字共有几种编码方法。
