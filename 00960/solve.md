要求每个字符串都是非降序的，其实就是求得所有字符串合体的最长非降子序列的长度，用简单的dp方法就可以，但是好像没法使用求LIS时候的log优化方法，所以复杂度为`size*len*len`
