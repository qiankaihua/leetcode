class Solution {
public:
    long long gridGame(vector<vector<int>>& grid) {
        int64_t line1sum = 0;
        size_t size = grid[0].size();
        for (size_t i = 0; i < size; ++ i) {
            line1sum += grid[0][i];
        }

        int64_t line1count = 0, line2count = 0, minLeft = INT64_MAX;
        for (size_t i = 0; i < size; ++i) { // 遍历拐点
            line1count += grid[0][i];
            minLeft = min(minLeft, max(line1sum - line1count, line2count));
            line2count += grid[1][i];
        }
        return minLeft;
    }
};