class Solution {
public:
    int minFlipsMonoIncr(string S) {
        int dp[2] = {0}, tmp;
        for (int i = 0; i < S.length(); ++i) {
            if (S[i] == '0') {
                dp[1] = min(dp[1], dp[0]) + 1;
                dp[0] = dp[0];
            } else {
                dp[1] = min(dp[0], dp[1]);
                dp[0] = dp[0] + 1;
            }
        }
        return min(dp[0], dp[1]);
    }
};