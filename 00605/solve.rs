impl Solution {
    pub fn can_place_flowers(flowerbed: Vec<i32>, n: i32) -> bool {
        let mut total = 0;
        let mut last = 0;
        for (i, v) in flowerbed.iter().enumerate() {
            if last == 0
                && *v == 0
                && ((i < flowerbed.len() - 1 && flowerbed[i + 1] == 0) || i == flowerbed.len() - 1)
            {
                total += 1;
                last = 1;
                continue;
            }
            last = *v;
        }
        total >= n
    }
}
