# Backspace String Compare

[题目链接](https://leetcode.com/problems/backspace-string-compare/)

<diff>Easy</diff>

给你两个字符串，`#` 表示退格，问你两个字符串最后相不相等
