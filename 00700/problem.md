# Search in a Binary Search Tree

[题目链接](https://leetcode.com/problems/search-in-a-binary-search-tree/)

<diff>Easy</diff>

给你一个二叉搜索树，让你找一个值及其子树。
