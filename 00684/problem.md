# Redundant Connection

[题目链接](https://leetcode.com/problems/redundant-connection/)

<diff>Medium</diff>

给N个点N条边，删除一条边使得图变成一棵树，如果有多个答案，返回最后的那条边。
