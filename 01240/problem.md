# Design Browser History

[题目链接](https://leetcode.com/problems/tiling-a-rectangle-with-the-fewest-squares/)

<diff>Hard</diff>

给你一个 n * m 的矩阵，问最少用多少个正方形能满铺。

1 <= n, m <= 13
