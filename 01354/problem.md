# Construct Target Array With Multiple Sums

[题目链接](https://leetcode.com/problems/construct-target-array-with-multiple-sums/)

<diff>Hard</diff>

给一个数组，问能否通过如下操作获得：

- 初始数组为 N 个 1
- 计算数组之和，并将其覆盖到数组的某一位
- 重复上一个运算
