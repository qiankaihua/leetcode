# Search in Rotated Sorted Array

[题目链接](https://leetcode.com/problems/search-in-rotated-sorted-array/)

<diff>Medium</diff>

给一个升序排列的数组, 但是数组会旋转 n 个长度, 给一个 target, 求 target 在数组的第几位, 不存在则返回 -1
