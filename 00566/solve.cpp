class Solution {
public:
    vector<vector<int>> matrixReshape(vector<vector<int>>& nums, int r, int c) {
        int n = nums.size();
        if (n == 0) return nums;
        int m = nums[0].size();
        if (n * m != r * c) return nums;
        vector<vector<int>> res(r, vector<int>(c));
        int pos;
        for (int i = 0; i < r; ++i) {
            for (int j = 0; j < c; ++j) {
                pos = i * c + j;
                res[i][j] = nums[pos / m][pos % m];
            }
        }
        return res;
    }
};