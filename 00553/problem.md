# Optimal Division

[题目链接](https://leetcode.com/problems/optimal-division/)

<diff>Medium</diff>

给一个数组，计算结果是 `a1 / a2 / a3 / ... / an`，你可以随意加括号，求一种使得结果最大的括号加法，括号不能有多余。
