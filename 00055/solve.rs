impl Solution {
    pub fn can_jump(nums: Vec<i32>) -> bool {
        let mut max = 0;
        let len = nums.len();
        for (idx, value) in nums.iter().enumerate() {
            if max < idx {
                return false;
            }
            max = max.max(idx + (*value) as usize);
            if max + 1 >= len {
                return true;
            }
        }
        if max + 1 >= len {
            return true;
        }
        return false;
    }
}
