class Solution {
public:
    long long maximumImportance(int n, vector<vector<int>>& roads) {
        vector<int> cnt(n, 0);
        for (auto edge : roads) {
            ++cnt[edge[0]];
            ++cnt[edge[1]];
        }
        sort(cnt.begin(), cnt.end());
        long long res = 0, value = 1;
        for (auto s : cnt) {
            res += value * s;
            ++value;
        }
        return res;
    }
};