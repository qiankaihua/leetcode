class Solution {
public:
    string reformat(string s) {
        vector<char> digits;
        vector<char> letters;
        for (int i = s.length() - 1; i >= 0; --i) {
            if (s[i] >= '0' && s[i] <= '9') digits.push_back(s[i]);
            else letters.push_back(s[i]);
        }
        if (abs((int)(digits.size() - letters.size())) > 1) return "";
        if (digits.size() > letters.size()) return format(digits, letters);
        else return format(letters, digits);
    }
    string format(vector<char>&large, vector<char>&small) {
        string s = "";
        for (int i = small.size() - 1; i >= 0; --i) {
            s += large[i];
            s += small[i];
        }
        if (large.size() > small.size()) s += large[large.size() - 1];
        return s;
    }
};