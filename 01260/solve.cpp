class Solution {
public:
    vector<vector<int>> shiftGrid(vector<vector<int>>& grid, int k) {
        int idx, n, m, sum;
        n = grid.size();
        if (n == 0) return grid;
        m = grid[0].size();
        if (m == 0) return grid;
        sum = n * m;
        vector<vector<int>> ans(n, vector<int>(m));
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                idx = (i * m + j + k) % sum;
                ans[idx / m][idx % m] = grid[i][j];
            }
        }
        return ans;
    }
};


// 原地操作版本
class Solution {
public:
    vector<vector<int>> shiftGrid(vector<vector<int>>& grid, int k) {
        int idx, n, m;
        n = grid.size();
        if (n == 0) return grid;
        m = grid[0].size();
        if (m == 0) return grid;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                grid[i][j] += 1000;
            }
        }
        int t = (1 << 12) - 1;
        int sum = n * m;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                idx = (i * m + j + k) % sum;
                grid[idx / m][idx % m] |= ((grid[i][j] & t) << 12);
            }
        }
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                grid[i][j] >>= 12;
                grid[i][j] -= 1000;
            }
        }
        return grid;
    }
};