# Delete Columns to Make Sorted II

[题目链接](https://leetcode.com/problems/delete-columns-to-make-sorted-ii/submissions/)

<diff>Medium</diff>

给一个字符串数组，长度都相同，问你最少删掉多少列才能使得字符串都按照字符序排序。
