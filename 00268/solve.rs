use std::convert::TryInto;
impl Solution {
    pub fn missing_number(nums: Vec<i32>) -> i32 {
        let mut sum: i32 = 0;
        let size: i32 = nums.len().try_into().unwrap();
        for i in nums {
            sum += i;
        }
        return (1 + size) * size / 2 - sum;
    }
}
