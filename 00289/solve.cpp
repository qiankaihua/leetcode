class Solution {
public:
    void gameOfLife(vector<vector<int>>& board) {
        int n = board.size();
        if (n == 0) return;
        int m = board[0].size(), cnt;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                cnt = 0;
                if (i != 0) {
                    if (j != 0) cnt += board[i - 1][j - 1] & 1;
                    if (j != m - 1) cnt += board[i - 1][j + 1] & 1;
                    cnt += board[i - 1][j] & 1;
                }
                if (i != n - 1) {
                    if (j != 0) cnt += board[i + 1][j - 1] & 1;
                    if (j != m - 1) cnt += board[i + 1][j + 1] & 1;
                    cnt += board[i + 1][j] & 1;
                }
                if (j != 0) cnt += board[i][j - 1] & 1;
                if (j != m - 1) cnt += board[i][j + 1] & 1;
                if (board[i][j] & 1) {
                    if (cnt == 2 || cnt == 3) board[i][j] += 2;
                } else {
                    if (cnt == 3) board[i][j] += 2;
                }
            }
        }
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                board[i][j] >>= 1;
            }
        }
    }
};