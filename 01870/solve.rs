impl Solution {
    pub fn min_speed_on_time(dist: Vec<i32>, hour: f64) -> i32 {
        let mut min = 1 as i32;
        let mut max = 10000001 as i32;
        let mut ok = false;
        while min < max {
            let mid = min + (max - min) / 2;
            println!("{}", mid);
            // sleep(Duration::new(1, 0));
            if Self::check_speed_ok(&dist, hour, mid) {
                max = mid;
                ok = true;
            } else {
                min = mid + 1;
            }
        }
        return if ok { max } else { -1 };
    }
    fn check_speed_ok(dist: &Vec<i32>, hour: f64, speed: i32) -> bool {
        let mut cost = 0;
        for i in 0..dist.len() - 1 {
            cost += dist[i] / speed + if dist[i] % speed != 0 { 1 } else { 0 };
            if cost as f64 > hour {
                return false;
            }
        }
        if cost as f64 + dist[dist.len() - 1] as f64 / speed as f64 > hour {
            return false;
        }
        return true;
    }
}
