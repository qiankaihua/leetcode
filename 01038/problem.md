# Binary Search Tree to Greater Sum Tree

[题目链接](https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/)

<diff>Medium</diff>

给一颗二叉树，将每个节点的值变为所有大于等于当前节点值的节点值之和。