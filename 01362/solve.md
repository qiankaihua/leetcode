我是这么做的：
先开方，另外一个数是给的数除以开方后的数，然后第二个数不断+1，如果满足条件，这个就是最接近的，直接返回，否则遍历到乘积超过原来的数+2之后，把第一个数-1，然后重新计算第一个数，继续循环，直到满足条件。

不过看到一种很快的做法，基本上是一个思路，不过人家是第二个数是直接算的，而不是一点点加出来的。

```cpp
class Solution {
public:
    vector<int> closestDivisors(int num) 
    {
        int n = sqrt(num) + 1;
        
        while(n)
        {
            if((num+2) % n <= 1 ) return { n  , (num+2)/n};
            n--;
        }
        
        return {};
    }
};
```