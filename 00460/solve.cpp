//#define debug 1
struct CacheNode {
    int key, val, cnt;
    CacheNode *nxt, *pre;
};
class LFUCache {
private:
    vector<CacheNode*> v;
    vector<CacheNode*> v_head;
    int cap, has;
    CacheNode *head, *tail;
    void update(CacheNode* now) {
        #ifdef debug
        cout << "update: " << now->key << endl;
        #endif
        if (v_head[now->cnt] == now) {
            if (now->nxt->cnt == now->cnt) {
                v_head[now->cnt] = now->nxt;
            } else {
                v_head[now->cnt] = nullptr;
            }
        }
        now->cnt = now->cnt + 1;
        CacheNode *p_node, *n_node;
        if (v_head[now->cnt] == nullptr) {
            v_head[now->cnt] = now;
            if (v_head[now->cnt - 1] == nullptr) {
                return;
            }
            n_node = v_head[now->cnt - 1];
        } else {
            n_node = v_head[now->cnt];
            v_head[now->cnt] = now;
        }
        now->nxt->pre = now->pre;
        now->pre->nxt = now->nxt;
        
        p_node = n_node->pre;
        
        now->nxt = p_node->nxt;
        now->pre = p_node;
        p_node->nxt->pre = now;
        p_node->nxt = now;
    }
    void freeNode(CacheNode* now) {
        #ifdef debug
        cout << "delete: " << now->key << endl;
        #endif
        --has;
        if (v_head[now->cnt] == now) {
            if (now->nxt->cnt == now->cnt) {
                v_head[now->cnt] = now->nxt;
            } else {
                v_head[now->cnt] = nullptr;
            }
        }
        v[now->key] = nullptr;
        now->pre->nxt = now->nxt;
        now->nxt->pre = now->pre;
        delete now;
    }
    void createNode(int key, int val) {
        #ifdef debug
        cout << "create: " << key << ' ' << val << endl;
        #endif
        ++has;
        CacheNode *now = new CacheNode;
        now->val = val;
        now->key = key;
        now->cnt = 1;
        v[key] = now;
        if (v_head[1] == nullptr) {
            now->nxt = tail;
            now->pre = tail->pre;
            tail->pre->nxt = now;
            tail->pre = now;
            v_head[1] = now;
        } else {
            CacheNode *n_node = v_head[1];
            now->nxt = n_node;
            now->pre = n_node->pre;
            n_node->pre->nxt = now;
            n_node->pre = now;
            v_head[1] = now;
        }
    }
    #ifdef debug
    void printList() {
        cout << "cnt head:";
        for (auto n : v_head) {
            if (n != nullptr) cout << "(" << n->key << ", " << n->val << ", " << n->cnt << ")";
        }
        cout << endl;
        cout << "list: head";
        CacheNode *tmp = head->nxt;
        while (tmp != tail) {
            cout << "->(" << tmp->key << ", " << tmp->val << ", " << tmp->cnt << ")";
            tmp = tmp->nxt;
        }
        cout << endl;
    }
    #endif
public:
    LFUCache(int capacity) {
        cap = capacity;
        has = 0;
        v.resize(10001, nullptr);
        v_head.resize(100001, nullptr);
        head = new CacheNode;
        tail = new CacheNode;
        head->nxt = tail;
        tail->pre = head;
    }
    ~LFUCache() {
        delete head;
        delete tail;
    }
    int get(int key) {
        #ifdef debug
        cout << "get: " << key << endl;
        #endif
        if (v[key] == nullptr) return -1;
        update(v[key]);
        #ifdef debug
        printList();
        #endif
        return v[key]->val;
    }
    
    void put(int key, int value) {
        #ifdef debug
        cout << "put: " << key << ' ' << value << endl;
        #endif
        if (cap == 0) return;
        if (v[key] != nullptr) {
            v[key]->val = value;
            update(v[key]);
            #ifdef debug
            printList();
            #endif
            return;
        }
        if (has == cap) {
            freeNode(tail->pre);
        }
        createNode(key, value);
        #ifdef debug
        printList();
        #endif
    }
};

/**
 * Your LFUCache object will be instantiated and called as such:
 * LFUCache* obj = new LFUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */