// 192 ms 68.5 MB
class Solution {
public:
    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
        vector<int> t;
        int index = 0;
        int ans = 0;
        int last_pos = 0;
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] >= k) {
                t.push_back(-1 * nums[i]);
                last_pos = t.size() - 1;
                continue;
            }
            int p = k / nums[i] * -1 + (k % nums[i] == 0 ? 1 : 0);
            int idx = lower_bound(t.begin() + last_pos, t.end(), p) - t.begin();
            last_pos = idx;
            ans += t.size() - idx + 1;
            int nxt_ind = (index + 1) % 2;
            int tmp = 0;
            if (nums[i] != 1) {
                for (int j = last_pos; j < t.size(); ++j) {
                    t[j] *= nums[i];
                }
            }
            t.push_back(-1 * nums[i]);
            index = nxt_ind;
        }
        return ans;
    }
};

// 双指针做法
// 76 ms 61.3 MB
class Solution {
public:
    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
        if (nums.size() <= 0) return 0;
        int ans = 0, fast = 0, slow = 0, product = nums[0];
        while (fast < nums.size() && slow < nums.size()) {
            if (product < k) {
                ++fast;
                if (fast >= slow) {
                    ans += fast - slow;
                }
                if (fast < nums.size()) {
                    product *= nums[fast];
                }
            } else {
                product /= nums[slow++];
            }
        }
        return ans;
    }
};