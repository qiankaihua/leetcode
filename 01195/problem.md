# Fizz Buzz Multithreaded

[题目链接](https://leetcode.com/problems/fizz-buzz-multithreaded/)

<diff>Medium</diff>

并发多线程问题，调用四个输出函数一次，要求输出n个值

从1到n

- 输出 fizzbuzz 当 i 能整除 3 和 5
- 输出 fizz 当 i 能整除 3 不能整除 5
- 输出 buzz 当 i 能整除 5 不能整除 3
- 输出 i 当 i 不能整除 3 和 5
