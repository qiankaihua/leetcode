# Number of Employees Who Met the Target

[题目链接](https://leetcode.com/problems/number-of-employees-who-met-the-target/)

<diff>Easy</diff>

给一个数组，代表每个员工的工作时长，另外给一个目标时长，求工作时间达到目标时长的员工数量。