class Solution {
public:
    int maxCoins(vector<int>& nums) {
        int size = nums.size();
        if (size == 0) return 0;
        vector<int> fix (size + 2, 1);
        int tmp = 0;
        vector<vector<int>> dp(size, vector<int>(size, 0));
        for (int i = 0; i < size; ++i) fix[i + 1] = nums[i];
        for (int i = 0; i < size; ++i) dp[i][i] = fix[i] * fix[i + 1] * fix[i + 2];
        for (int len = 2; len <= size; ++len) {
            for(int i = 0; i + len - 1 < size; ++i) {
                for (int k = 0; k < len; ++k) {
                    if (k == 0) {
                        tmp = fix[i] * fix[i + 1] * fix[i + len + 1] + dp[i + 1][i + len - 1];
                    } else if (k == len - 1) {
                        tmp = fix[i] * fix[i + len] * fix[i + len + 1] + dp[i][i + len - 2];
                    } else {
                      tmp = fix[i] * fix[i + k + 1] * fix[i + len + 1] + dp[i][i + k - 1] + dp[i + k + 1][i + len - 1];
                    }
                    dp[i][i + len - 1] = max(dp[i][i + len - 1], tmp);
                }
            }
        }
        return dp[0][size - 1];
    }
};