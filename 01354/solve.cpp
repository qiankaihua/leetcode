class Solution {
public:
    bool isPossible(vector<int>& target) {
        long long sum = 0;
        for (int i = 0; i < target.size(); ++i) sum += target[i];
        if (target.size() == 1) return target[0] == 1;
        priority_queue<int> pq(target.begin(), target.end());
        long long top, nsum;
        while (pq.top() != 1) {
            top = pq.top();
            pq.pop();
            sum -= top;
            if (top <= sum) return false;
            nsum = top % sum + sum;
            top %= sum;
            if (sum == 1) top = 1;
            if (top < 1) return false;
            sum = nsum;
            pq.push(top);
        }
        return true;
    }
};