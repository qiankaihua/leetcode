/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode* root) {
        vector<vector<int>> ans;
        if (root == NULL) return ans;
        vector<TreeNode*>nodes;
        nodes.push_back(root);
        int len = 1, nLen;
        bool dir = true;
        while (len) {
            vector<int> tmp;
            vector<TreeNode*>nNodes;
            nLen = 0;
            for (int i = 0; i < len; ++i) {
                tmp.push_back(nodes[i]->val);
                if (nodes[i]->left) nNodes.push_back(nodes[i]->left), ++nLen;
                if (nodes[i]->right) nNodes.push_back(nodes[i]->right), ++nLen;
            }
            if (!dir) {
                reverse(tmp.begin(), tmp.end());
            }
            dir = !dir;
            len = nLen;
            nodes = nNodes;
            ans.push_back(tmp);
        }
        return ans;
    }
};