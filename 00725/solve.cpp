/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    vector<ListNode*> splitListToParts(ListNode* root, int k) {
        ListNode *tmp = root, *last;
        int cnt = 0;
        while (tmp != 0) {
            ++cnt;
            tmp = tmp->next;
        }
        vector<ListNode*> ans;
        int a = cnt / k, b = cnt % k;
        tmp = root;
        for (int i = 0; i < b; ++i) {
            ans.push_back(tmp);
            for (int j = 0; j < a; ++j) {
                tmp = tmp->next;
            }
            last = tmp;
            tmp = tmp->next;
            last->next = NULL;
        }
        for (int i = b; i < k; ++i) {
            if (a == 0) {
                ans.push_back(NULL);
                continue;
            }
            ans.push_back(tmp);
            for (int j = 0; j < a - 1; ++j) {
                tmp = tmp->next;
            }
            last = tmp;
            tmp = tmp->next;
            last->next = NULL;
        }
        return ans;
    }
};