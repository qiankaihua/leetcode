# Path with Maximum Gold

[题目链接](https://leetcode.com/problems/path-with-maximum-gold/)

<diff>Medium</diff>

给一个数组，可以从任意位置开始上下左右走，不能走到0的位置，走到的任意位置会获取当前位置的值，并且将值变为0，问最大能获取的值是多少