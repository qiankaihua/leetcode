class Solution {
public:
    vector<vector<int>> minimumAbsDifference(vector<int>& arr) {
        sort(arr.begin(), arr.end());
        vector<vector<int>> ans;
        int min = INT_MAX, diff;
        for (int i = 0; i < arr.size() - 1; ++i) {
            diff = arr[i + 1] - arr[i];
            if (diff == min) {
                ans.push_back(vector<int>{arr[i], arr[i + 1]});
            } else if (diff < min) {
                ans.clear();
                min = diff;
                ans.push_back(vector<int>{arr[i], arr[i + 1]});
            }
        }
        return ans;
    }
};