# Maximum Total Importance of Roads

[题目链接](https://leetcode.com/problems/maximum-total-importance-of-roads/)

<diff>Medium</diff>

给 n 座城市,标号 0~`n-1`, 后面给一系列边
需要你给 n 座城市分配价值,从 1-n,价值总和为城市价值乘以相连城市的数量之和
