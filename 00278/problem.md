# First Bad Version

[题目链接](https://leetcode.com/problems/first-bad-version/)

<diff>Easy</diff>

你有n个版本，从第m个版本开始是坏版本，找到第一个坏版本

已经有一个API `bool isBadVersion(version)` 来判断版本是否是坏版本
