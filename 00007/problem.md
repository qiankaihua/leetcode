# Reverse Integer

[题目链接](https://leetcode.com/problems/reverse-integer/)

<diff>Medium</diff>

给一个数字, 按照 10 进制翻转, 123 -> 321, -123 -> -321, 如果翻转后的数字超过 i32 的范围, 返回 0
