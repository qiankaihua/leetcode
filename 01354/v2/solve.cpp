class Solution {
public:
    bool isPossible(vector<int>& target) {
        priority_queue<int, vector<int>, less<int>> q;
        int64_t sum = 0;
        for (auto num : target) {
            q.push(num);
            sum += num;
        }
        int tmp;
        while (!q.empty()) {
            tmp = q.top();
            q.pop();
            sum -= tmp;
            if (tmp == 1 || sum == 1) {
                return true;
            }
            if (tmp <= sum || sum == 0 || tmp % sum == 0) {
                return false;
            }
            tmp %= sum;
            q.push(tmp);
            sum += tmp;
        }
        return true;
    }
};