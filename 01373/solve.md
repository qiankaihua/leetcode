遍历树，记录子树最大值最小值用来判断是否是二叉搜索树，记录子树和来计算父节点树的和，记录最大的ansSum来和中间值来进行比较计算，最后返回的就是全局最大。

下面是耗时较短的写法，`array<int, 3>` 分别代表子树和、最大值、最小值，全局最大放在类成员 `max_sum` 中

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int max_sum = 0;
    int maxSumBST(TreeNode* root) {
        dfs(root);
        return max_sum;
    }
    
private:
    array<int,3> dfs(TreeNode* root) {
        auto l = root->left ? dfs(root->left) : array<int, 3>{0, root->val, root->val-1};
        auto r = root->right ? dfs(root->right) : array<int, 3>{0, root->val+1, root->val};
        if (l[2] < root->val && root->val < r[1]) {
            max_sum = max(max_sum, root->val+l[0]+r[0]);
            return {root->val+l[0]+r[0], l[1], r[2]};
        }
        return {0, INT_MIN, INT_MAX};
    }
};
```