class Solution {
public:
    int latestDayToCross(int row, int col, vector<vector<int>>& cells) {
        // 初始化并查集
        int size = (2 + row) * col;
        uset.resize(size);
        mark.resize(size, 0);
        for (int i = 0; i < size; ++i) uset[i] = i;
        // 将上面和下面额外增加的一行合并成一个集合
        mark[0] = mark[size - col] = 1;
        for (int i = 1; i < col; ++i) {
            // 首行
            uset[i] = uset[0];
            mark[i] = 1;
            // 尾行
            uset[i + size - col] = uset[size - col];
            mark[i + size - col] = 1;
        }
        // 从后面往前遍历，判断减少一个格子之后是否连通（是否处于同一个并查集）
        int r, c, idx, now;
        for (int i = cells.size() - 1; i >= 0; --i) {
            r = cells[i][0];
            c = cells[i][1];
            now = r * col + c - 1;
            mark[now] = 1;
            // 上
            idx = (r - 1) * col + c - 1;
            if (mark[idx] == 1) uset[find(idx)] = find(now);
            // 下
            idx = (r + 1) * col + c - 1;
            if (mark[idx] == 1) uset[find(idx)] = find(now);
            // 左
            if (c > 1) {
                idx = r * col + c - 2;
                if (mark[idx] == 1) uset[find(idx)] = find(now);
            }
            // 右
            if (c < col) {
                idx = r * col + c;
                if (mark[idx] == 1) uset[find(idx)] = find(now);
            }
            // cout << i << endl;
            // print(row, col);
            // 判断是否连通
            if (find(0) == find(size - 1)) {
                return i;
            }
        }
        return 0;
    }
private:
    vector<int> uset;
    vector<int> mark;
    int find(int i) {
        return uset[i] == i ? i : uset[i] = find(uset[i]);
    }
    void print(int row, int col) {
        for (int i = 0; i < row + 2; ++i) {
            for (int j = 0; j < col; ++j) {
                cout << uset[i * col + j] << '\t';
            }
            cout << endl;
        }
        cout << endl;
        cout << "--------------------" << endl;
    }
};