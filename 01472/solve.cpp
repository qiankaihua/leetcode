class BrowserHistory {
public:
    vector<string> v;
    int n;
    int max;
    BrowserHistory(string homepage) {
        n = 0;
        max = 0;
        v.push_back(homepage);
    }
    
    void visit(string url) {
        // cout << "visit " << n << ' ' << max << endl;
        if (n < max) {
            n++;
            v[n] = url;
            max = n;
            return;
        }
        n++;
        max = n;
        if (v.size() < n + 1) {
            v.push_back(url);
        } else {
            v[n] = url;
        }
        return;
    }
    
    string back(int steps) {
        // cout << "back " << n << ' ' << max << endl;
        if (steps > n) {
            n = 0;
            return v[0];
        }
        n -= steps;
        return v[n];
    }
    
    string forward(int steps) {
        // cout << "forward " << n << ' ' << max << endl;
        if (n + steps <= max) {
            n += steps;
            return v[n];
        }
        n = max;
        return v[n];
    }
};

/**
 * Your BrowserHistory object will be instantiated and called as such:
 * BrowserHistory* obj = new BrowserHistory(homepage);
 * obj->visit(url);
 * string param_2 = obj->back(steps);
 * string param_3 = obj->forward(steps);
 */