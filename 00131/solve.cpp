class Solution {
public:
    string s;
    int size;
    void dfs(vector<vector<string>> &ans, vector<string> &tmp, int pos, int level) {
        if (pos == size) {
            ans.push_back(tmp);
            return;
        }
        for (int now = pos + 1; now <= size; ++now) {
            if (isPalindrome(pos, now - 1)) {
                if (tmp.size() < level + 1) {
                    tmp.push_back(s.substr(pos, now - pos));
                } else {
                    tmp[level] = s.substr(pos, now - pos);
                }
                dfs(ans, tmp, now, level + 1);
            }
        }
        while (tmp.size() > level) tmp.pop_back();
    }
    bool isPalindrome(int start, int end) {
        while (start < end) {
            if (s[start] != s[end]) return false;
            ++start;
            --end;
        }
        return true;
    }
    vector<vector<string>> partition(string s) {
        vector<vector<string>> ans;
        vector<string> tmp;
        this->s = s;
        size = s.length();
        dfs(ans, tmp, 0, 0);
        return ans;
    }
};