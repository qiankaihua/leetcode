# String Matching in an Array

[题目链接](https://leetcode.com/problems/string-matching-in-an-array/)

<diff>Easy</diff>

给一个字符串数组，判断哪些字符串是另外一个的子串。
