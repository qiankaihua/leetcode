struct node {
    int id;
    node* nxt;
};
class Solution {
public:
    int findTheWinner(int n, int k) {
        if (n <= 1) return n;
        node *head = new node, *pre;
        head->id = 0;
        pre = head;
        for (int i = 0; i < n; ++i) {
            node *nd = new node;
            nd->id = i + 1;
            pre->nxt = nd;
            pre = pre->nxt;
        }
        pre->nxt = head->nxt;
        node *now = head->nxt;
        for (int t = 1; t < n; ++t) {
            for (int i = 1; i < k; ++i) {
                now = now->nxt;
                pre = pre->nxt;
            }
            now = now->nxt;
            pre->nxt = now;
        }
        return pre->id;
    }
};