impl Solution {
    pub fn reverse(x: i32) -> i32 {
        let mut res = 0 as i64;
        let mut ori = x.abs();
        while ori > 0 {
            res *= 10;
            res += (ori % 10) as i64;
            ori /= 10;
        }
        if x < 0 {
            res *= -1;
        }
        if res > std::i32::MAX as i64 || res < std::i32::MIN as i64 {
            return 0;
        }
        return res as i32;
    }
}
