# Flood Fill

[题目链接](https://leetcode.com/problems/flood-fill/)

<diff>Easy</diff>

给一个像素网格，选择一个点改变颜色，上下左右四个格子都是联通的，联通的相同颜色的块都会变成新的颜色。
