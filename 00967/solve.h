#ifdef LEETCODE_INCLUDE_TEST
#include "../0test/include.h"
#endif

class Solution {
public:
    vector<int> numsSameConsecDiff(int n, int k) {
        vector<int> result;
        int pow = 1;
        for (int i = 1; i < n; ++i) {
            pow *= 10;
        }

        int nxtPow = pow / 10;
        for (int i = 1; i < 10; ++i) {
            dfs(n, k, pow * i, 1, i, nxtPow, result);
        }
        return result;
    }

private:
    void dfs(int n, int k, int now, int index, int last, int pow, vector<int> &result) {
        if (index == n) {
            result.push_back(now);
            return;
        }
        int nxtPow = pow / 10;
        if (last - k >= 0) {
            dfs(n, k, now + pow * (last - k), index + 1, last - k, nxtPow, result);
        }
        if (last + k <= 9 && k != 0) {
            dfs(n, k, now + pow * (last + k), index + 1, last + k, nxtPow, result);
        }
    }
};