/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
//#define DEBUG
class Solution {
public:
    int maxSumBST(TreeNode* root) {
        int maxS = 0, sum = 0, mx, mn;
        checkBST(root, maxS, sum, mx, mn);
        return max(maxS, 0);
    }
    // 返回是否是BST
    bool checkBST(TreeNode* root, int& maxSum, int& sum, int& maxx, int& minn) {
        if (root == NULL) {
            maxSum = 0;
            sum = 0;
            maxx = INT_MIN;
            minn = INT_MAX;
            return true;
        }
        int left = 0, right = 0, ls = 0, rs = 0, lmax, lmin, rmax, rmin;
        bool bl, br;
        bl = checkBST(root->left, left, ls, lmax, lmin);
        br = checkBST(root->right, right, rs, rmax, rmin);
        #ifdef DEBUG
            cout << "root: " << root->val << endl;
        #endif
        #ifdef DEBUG
            cout << "c: " << left << ' ' << right << endl;
            cout << "cs: " << ls << ' ' << rs << endl;
            cout << "is bst: " << bl << ' ' << br << endl;
        #endif
        bool flag = bl && br && (root->left == NULL || root->val > lmax) && (root->right == NULL || root->val < rmin);
        sum = ls + rs + root->val;
        if (flag) {
            maxx = max(rmax, root->val);
            minn = min(lmin, root->val);
            maxSum = max(max(sum, maxSum), max(left, right));
            #ifdef DEBUG
                cout << "r is bst: " << maxSum << ' ' << sum << endl;
            #endif
            return true;
        }
        maxSum = max(maxSum, max(left, right));
        #ifdef DEBUG
            cout << "r is not bst: " << maxSum << ' ' << sum << endl;
        #endif
        return false;
    }
};