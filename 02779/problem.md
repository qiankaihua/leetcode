# Maximum Beauty of an Array After Applying Operation

[题目链接](https://leetcode.com/problems/maximum-beauty-of-an-array-after-applying-operation)

<diff>Medium</diff>

给一个数组 num 和一个 k, 数组中每个数都可以变成 [n - k, n + k] 中的任意一个数.
计算变化后最多有多少个数相同.