用Group by语句来合并相同的工资，select ... order by Salary desc limit 1, 1 获取工资第二高的，但是这时如果没有的话则会返回空，而不是NULL，网上查了之后学到可以用以下代码实现

```sql
SELECT (原 select 语句) AS `alias`
```

当然，也可以不用 group by

```sql
select 
(select distinct salary from employee
 order by salary desc
 limit 1 offset 1) as secondhighestsalary
```

```sql
select Max(salary)  as SecondHighestSalary from employee where salary < (select Max(Salary) from employee)
```