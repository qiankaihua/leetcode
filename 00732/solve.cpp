class MyCalendarThree {
private:
    struct Node {
        int point, flag;
        Node *nxt;
        Node(int p, int f) {
            point = p;
            flag = f;
            nxt = NULL;
        }
    };
    Node *head;
    void insert(Node *newNode) {
        Node *tmp = head;
        while (tmp) {
            if (tmp->nxt == NULL || tmp->nxt->point >= newNode->point) {
                newNode->nxt = tmp->nxt;
                tmp->nxt = newNode;
                return;
            }
            tmp = tmp->nxt;
        }
    }
public:
    MyCalendarThree() {
        head = new Node(-1, 0);
    }
    ~MyCalendarThree() {
        Node *nxt;
        while (head) {
            nxt = head->nxt;
            delete head;
            head = nxt;
        }
    }
    int book(int start, int end) {
        insert(new Node(start, 1));
        insert(new Node(end, -1));
        int ans = 0, now = 0, point;
        Node *tmp = head;
        while (tmp) {
            if (tmp->point != point) {
                ans = max(ans, now);
                point = tmp->point;
            }
            now += tmp->flag;
            tmp = tmp->nxt;
        }
        ans = max(ans, now);
        return ans;
    }
};

/**
 * Your MyCalendarThree object will be instantiated and called as such:
 * MyCalendarThree* obj = new MyCalendarThree();
 * int param_1 = obj->book(start,end);
 */