class FizzBuzz {
private:
    int n;
    bool stop;
    std::atomic<int> now;

public:
    FizzBuzz(int n) {
        this->n = n;
        stop = false;
        this->now = 1;
    }

    // printFizz() outputs "fizz".
    void fizz(function<void()> printFizz) {
        while (!stop) {
            int a = this->now;
            if (a % 3 == 0 && a % 5 != 0 && a <= n) {
                printFizz();
                this->now = a + 1;
            }
            if (a > n) this->stop = true;
        }
    }

    // printBuzz() outputs "buzz".
    void buzz(function<void()> printBuzz) {
        while (!stop) {
            int a = this->now;
            if (a % 3 != 0 && a % 5 == 0 && a <= n) {
                printBuzz();
                this->now = a + 1;
            }
        }
    }

    // printFizzBuzz() outputs "fizzbuzz".
	void fizzbuzz(function<void()> printFizzBuzz) {
        while (!stop) {
            int a = this->now;
            if (a % 3 == 0 && a % 5 == 0 && a <= n) {
                printFizzBuzz();
                this->now = a + 1;
            }
        }
    }

    // printNumber(x) outputs "x", where x is an integer.
    void number(function<void(int)> printNumber) {
        while (!stop) {
            int a = this->now;
            if (a % 3 != 0 && a % 5 != 0 && a <= n) {
                printNumber(a);
                this->now = a + 1;
            }
        }
    }
};