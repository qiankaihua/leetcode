class Solution {
public:
    string predictPartyVictory(string senate) {
        int size = senate.length();
        int cnt = 0;
        char now;
        bool changed;
        while (1) {
            changed = false;
            for (int i = 0; i < size; ++i) {
                if (senate[i] == '!') continue;
                if (now == senate[i]) ++cnt;
                else {
                    if (cnt == 0) {
                        changed = true;
                        now = senate[i];
                        cnt = 1;
                    } else {
                        senate[i] = '!';
                        --cnt;
                    }
                }
            }
            if (!changed) break;
        }
        if (now == 'R') return "Radiant";
        else return "Dire";
    }
};