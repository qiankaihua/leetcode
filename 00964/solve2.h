class Solution {
public:
    unordered_map<string, int> seen;

    int solve(int x, int cur, int depth) {
        if (cur == 0)
            return -1;          // -1 为去除开头的正号。

        int usage = depth;
        if (depth == 0) {
            usage = 2;
        }

        if (cur == 1)
            return usage - 1;  // -1 为去除开头的正号。

        string pr(to_string(depth) + " " + to_string(cur));
        if (seen.find(pr) != seen.end())
            return seen[pr];

        int div = cur / x, r = cur % x;
        if (r == 0)
            return seen[pr] = solve(x, div, depth + 1);

        return seen[pr] = min(
            solve(x, div, depth+1)+usage*r,
            solve(x, div+1, depth+1)+usage*(x-r)
        );
    }

    int leastOpsExpressTarget(int x, int target) {
        return solve(x, target, 0);
    }
};