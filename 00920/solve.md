看到需要mod 1e9+7，很容易想到dp，那么需要考虑的就是dp数组表达什么含义

我这里用了二维数组 `dp[L][N]`, 第一个代表当前列表选取了多少首歌，第二个代表当前用的歌曲种类数，转移方程如下：

1. 不添加新歌曲，则种类数为 `dp[i][j] = dp[i - 1][j] * (j - K > 0 ? j - K : 0)`, 是长度 -1 的种类数乘上 歌曲种类 -k（最小为0），由于两首相同的歌中间必须间隔K首歌，所以前k首是不存在相同的歌曲的，歌曲种类可以直接减去K
2. 添加新歌曲，则种类数为 `dp[i][j] = dp[i - 1][j - 1] * (N - j + 1)`, 相当于长度-1歌曲种类-1的情况乘上剩余歌曲种类数。

两者相加就是dp一个单元的结果，返回`dp[L][N]`就是答案，注意中间过程的取模即可。
