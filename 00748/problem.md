# Shortest Completing Word

[题目链接](https://leetcode.com/problems/shortest-completing-word/)

<diff>Easy</diff>

给你一个模板串和一堆测试串，让你找出最短的，拥有所有模板串中的字符的测试串，不区分大小写，有多个满足条件的返回第一个。
