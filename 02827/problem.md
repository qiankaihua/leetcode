# Number of Beautiful Integers in the Range

[题目链接](https://leetcode.com/problems/number-of-beautiful-integers-in-the-range/)

<diff>Hard</diff>

一个数，如果每位上奇数和偶数数量一样并且能整除k，则认为是一个漂亮数。
问 `[l, r]` 中的漂亮数有几个