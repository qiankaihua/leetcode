# Next Greater Element III

[题目链接](https://leetcode.com/problems/next-greater-element-iii/)

<diff>Medium</diff>

给一个32位的数字，问你是否存在一个`32位`的数字比当前数字大，如果没有返回-1，如果有，则返回一个最小的比当前数字大的`32位`数字。
