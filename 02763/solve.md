枚举开头，计算以这个开头的子数组 `imbalance number` 和。
从长度为2的数组往后不断添加一位，记录访问过的数字和当前的 `imbalance number` ，此时有四种可能：

1. 新加入的数字已经在记录里面：不影响 `imbalance number`，直接在答案中加入当前的 `imbalance number`
2. 新加入的数字加减一都存在数字：这时候需要给 `imbalance number` 减一，再将当前的 `imbalance number` 加入答案，因为新加入的数字打破了之前已有的一对 `imbalance number`
3. 新加入的数字加减一其中一个存在数字：不影响 `imbalance number`，直接在答案中加入当前的 `imbalance number`
4. 新加入的数字加减一都不存在数字：这时候需要给 `imbalance number` 加一，再将当前的 `imbalance number` 加入答案，因为之前前后两个数只构成了一对 `imbalance number` ，而新加入数字后构成了两对。