class Solution {
public:
    vector<int> loudAndRich(vector<vector<int>>& richer, vector<int>& quiet) {
        int size = quiet.size();
        vector<vector<int>> mp(size);
        vector<int>ans(size, -1);
        for (int i = richer.size() - 1; i >= 0; --i) {
            mp[richer[i][1]].push_back(richer[i][0]);
        }
        for (int i = 0; i < size; ++i) {
            dfs(ans, mp, i, quiet);
        }
        return ans;
    }
    int dfs(vector<int> &ans, vector<vector<int>> &mp, int pos, vector<int> &quiet) {
        if (ans[pos] != -1) return ans[pos];
        ans[pos] = pos;
        for (int i = mp[pos].size() - 1; i >= 0; --i) {
            if (quiet[ans[pos]] > quiet[dfs(ans, mp, mp[pos][i], quiet)]) {
                ans[pos] = ans[mp[pos][i]];
            }
        }
        return ans[pos];
    }
};