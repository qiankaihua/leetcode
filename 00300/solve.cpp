class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        int len = nums.size();
        if (len == 0) return 0;
        vector<int> dp(len, 0x7fffffff);
        int now = 0, idx;
        for (int i = 0; i < len; ++i) {
            idx = getFirLargeIndex(dp, now, nums[i]);
            cout << idx <<endl;
            if (dp[idx] == 0x7fffffff) ++now;
            dp[idx] = nums[i];
        }
        return now;
    }
    int getFirLargeIndex(vector<int>& nums, int len, int d) {
        if (len == 0) return 0;
        int l = 0, r = len;
        int m;
        while (l < r) {
            m = l + (r - l) / 2;
            if (nums[m] == d) {
                return m;
            }
            if (nums[m] < d) {
                l = m + 1;
            } else {
                r = m;
            }
        }
        return l;
    }
};