class Solution {
public:
    int findNumbers(vector<int>& nums) {
        int res = 0, tmp, cnt;
        for (auto num : nums) {
            tmp = num;
            cnt = 0;
            while(tmp) {
                tmp /= 10;
                ++cnt;
            }
            if (cnt % 2 == 0) ++res;
        }
        return res;
    }
};