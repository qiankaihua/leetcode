# Last Stone Weight

[题目链接](https://leetcode.com/problems/last-stone-weight/)

<diff>Easy</diff>

给一堆数字，每次取出最大的两个，再把他们之差放回，问最后剩下的数字多大。
