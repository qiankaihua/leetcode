# Find a Corresponding Node of a Binary Tree in a Clone of That Tree

[题目链接](https://leetcode.com/problems/find-a-corresponding-node-of-a-binary-tree-in-a-clone-of-that-tree/)

<diff>Medium</diff>

给两棵树，第二颗是第一颗的拷贝，给出第一颗树中的一个节点的地址，找出第二棵树中对应的节点地址。
如果节点可以有重复value怎么解。